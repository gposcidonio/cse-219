\documentclass[]{article}
\usepackage{graphicx}
\usepackage{pdfpages}
\usepackage[margin = 0.5 in]{geometry}

\usepackage{listings}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.8}

\lstset{frame=tb,
  language=HTML,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}


\DeclareGraphicsExtensions{.pdf,.png,.jpg}


\setcounter{tocdepth}{5}
\begin{document}

\begin{titlepage}
	\begin{flushleft}
	\vspace*{2.5in}
	\textsc{\Huge Journey Through Europe}
	\\[0.2in]
	\textsc{\LARGE Software Design Description}
	\\[4in]
	\textbf{Author: }  Gustavo Poscidonio\\
	\hspace*{48pt} October 2014\\
	\hspace*{50pt} Version 1.0\\[0.2in]
	\textbf{Abstract: }This document provides the software design for Journey Through Europe, a classic board game that will be
	\\ \hspace*{50pt} adapted to play on the computer.\\[0.2in]
	\textbf{Based on IEEE Std 1016-2009 document format.}\\[0.2in]
	Copyright $\copyright$ Gustavo Poscidonio 2014
	\end{flushleft}
\end{titlepage}

\section{Introduction}
This is the Software Design Description (SDD) for the Journey Through Europe$^{TM}$ application. Note that this document is based on the IEEE Standard 1016-2009 recommendation for software design.

\subsection{Purpose}
This document is to serve as the blueprint for the construction of the Journey Through Europe application. This design will use UML class diagrams to provide complete detail regarding all packages, classes, instance variables, class variables, and method signatures needed to build the application. In addition, UML Sequence diagrams will be used to specify object interactions post-initialization of the application, meaning in response to user interactions or timed events.

\subsection{Scope}
Journey Through Europe will be one mini-game in a series of "Journey Through" mini-games. As such, the implementation should minimize the duplication of work and maximize the reusability of code.This document contains descriptions of how this should be done. note that Java is the target language for this software design.

\subsection{Definitions, Acronyms, and Abbreviations}

\textbf{Class Diagram: }A UML document format that describes classes graphically. Specifically, it describes theirinstance variables, method headers, and relationships to other classes.\\\\
\textbf{IEEE: }Institute of Electrical and Electronics Engineers, the “world's largest professional association for the advancement of technology”.\\\\
\textbf{Framework: }In an object-oriented language, a collection of classes and interfaces that collectively provide a service for building applications or additional frameworks all with a common need.\\\\
\textbf{Java: }A high-level programming language that uses a virtual machine layer between the Java application and the hardware to provide program portability.\\\\
\textbf{Journey Through Europe: }A board game published by Ravensburger where the object of the game is to travel to various cities in Europe and finally make your way back to your home city. This serves as the model for the Journey Through Europe application.\\\\
\textbf{Sequence Diagram: }A UML document format that specifies how object methods interact with one another.\\\\
\textbf{UML: }Unified Modeling Language, a standard set of document formats for designing software graphically.

\subsection{References}
\textbf{IEEE Std 1016-2009} – IEEE Standard for Information Technology – Systems Design – Software Design Descriptions\\\\
\textbf{Journey Through Europe SRS} – Debugging Enterprises’ Software Requirements Specification for the Journey Through Europe application.

\subsection{Overview}
This Software Design Description document provides a working design for the Journey Through Europe software application as described in the Journey Through Europe Software Requirements Specification. Note that all parties in the implementation stage must agree upon all connections between components before proceeding with the implementation stage. Section 2 of this document will provide the Package-Level Viewpoint, specifying the packages and frameworks to be designed. Section 3 will provide the Class-Level Viewpoint, using UML Class Diagrams to specify how the classes should be constructed. Section 4 will provide the Method-Level System Viewpoint, describing how methods will interact with one another. Section 5 provides deployment information like file structures and formats to use. Section 6 provides a Table of Contents, an Index, and References. Note that all UML Diagrams in this document were created using LucidChart.

\newpage
\section{Package-Level Design Viewpoint}
As mentioned, this design will encompass the Journey Through Europe game application. In building it we will heavily rely on the Java API to provide services. Following are descriptions of the components to be built.
\begin{center}
\includegraphics[scale = 0.7]{pkg_lvl_design.png}
\end{center}
\subsection{Java API Usage}
The application will be using the following classes in its implementation.

\begin{center}
	\includegraphics[scale = 0.25]{api_summary.png}
\end{center}

\subsection{Java API Usage Descriptions}
\begin{center}
	\begin{tabular}{|l|l|}
	\hline\\
	Class/Interface & Use \\\hline
	ActionEvent & For getting information about an action event like which button was pressed. \\\hline
	ActionListener & For responding to an action event, like a button press. \\\hline
	MouseEvent & For getting information about a mouse event, like where the button was pressed.\\\hline
	MouseListener & For responding to a mouse event. like a mouse button press.\\\hline
	\end{tabular}\\[0.1in]
	Table 2.1: Uses for classes in the Java API's java.awt.event package.\\[0.2in]
	\newpage
	\begin{tabular}{|l|l|}
	\hline\\
	Class/Interface & Use\\\hline
	Image & For holding the images of the cards, dice and board.\\\hline
	\end{tabular}\\[0.1in]
	Table 2.2: Uses for classes in the Java API's java.awt.image package.\\[0.2in]
	
	\begin{tabular}{|l|l|}
	\hline\\
	Class/Interface & Use\\\hline
	HashMap & For holding the City and Card objects that represent the actual cities and cards.\\\hline
	ArrayList & For holding lists of cards and players. \\\hline
	ArrayDeque & For managing the turns of all players in the game.\\\hline
	\end{tabular}\\[0.1in]
	Table 2.3: Uses for classes in the Java API's java.util package.\\[0.2in]

	\begin{tabular}{|l|l|}
	\hline\\
	Class/Interface & Use\\\hline
	BorderPane & For laying things out in a top, bottom, left, right, center layout. \\\hline
	StackPane & For laying things out on top of one another like a stack. \\\hline
	Pane & General container for storing JavaFX nodes.\\\hline
	VBox & For arranging items vertically in a static area on the screen.\\\hline
	GridPane & For arranging nodes in a grid. \\\hline
	Canvas & For rendering the game screen \\\hline
	\end{tabular}\\[0.1in]
	Table 2.4: Uses for classes in the Java API's javafx.scene.layout package.\\[0.2in]
	
	\begin{tabular}{|l|l|}
	\hline\\
	Class/Interface & Use\\\hline
	Button & For performing a predefined functionality upon a click at a particular location. \\\hline
	RadioButton & For selecting one particular item from a group of items. \\\hline
	ToggleGroup & For assigning RadioButtons to groups. \\\hline
	Label & For displaying information to the user. \\\hline
	TextField & For accepting input from the user. \\\hline
	\end{tabular}\\[0.1in]
	Table 2.5: Uses for classes in the Java API's javafx.scene.control package.
\end{center}

\section{Class-Level Design Viewpoint}
The following several pages will outline how each of the different classes interact with one another to create this program. The rest of this page is intentionally left blank.

\includepdf[pages={-}]{class_diag_1.pdf}
\includepdf[pages={-}]{class_diag_2.pdf}
\includepdf[pages={-}]{class_diag_3.pdf}

\section{Method-Level Design Viewpoint}
Now that the general architecture of the classes has been determined, it is time to specify how data will flow through the system. The following UML Sequence Diagrams describe the methods called within the code to be developed in order to provide the appropriate event responses. The rest of this page is intentionally left blank.

\includepdf[pages={-}]{seq_diag.pdf}

\section{File Structure and Formats}
All the data relevant to creating a new game will be stored in XML files that follow the format below. The code below is from another application that used a similar implementation of housing information in XML files.

\begin{lstlisting}
<?xml version="1.0" encoding="UTF-8"?>
<!--
    The properties in the  file are loaded when the Sokoban game application
    first loads.
-->
<properties>
    <property_list>    
        <property name="SPLASH_SCREEN_TITLE_TEXT"   value="Select a Level"></property>
        <property name="WINDOW_WIDTH"               value="820"></property>
        <property name="WINDOW_HEIGHT"              value="820"></property>
        <property name="SPLASH_SCREEN_IMAGE_NAME"   value="SokobanSplashScreen.png"></property>
        <property name="DATA_PATH"                  value="./data/"></property>
        <property name="IMG_PATH"                   value="./images/"></property>
        <property name="LETTERS_FONT_FAMILY"        value="monospaced"></property>
        <property name="LETTERS_FONT_SIZE"          value="24"></property>
        <property name="DEFAULT_YES_TEXT"           value="Yes"></property>
        <property name="DEFAULT_NO_TEXT"            value="No"></property>
        <property name="DEFAULT_EXIT_TEXT"          value="Are you sure you wish to Quit?"></property>
    </property_list>
    <property_options_list>
        <property_options name="LEVEL_OPTIONS">
            <option>Level 1</option>
            <option>Level 2</option>
            <option>Level 3</option>
            <option>Level 4</option>
            <option>Level 5</option>
            <option>Level 6</option>
            <option>Level 7</option>
        </property_options>
        <property_options name="LEVEL_IMAGE_NAMES">
            <option>level_1.png</option>
            <option>level_2.png</option>
            <option>level_3.png</option>
            <option>level_4.png</option>
            <option>level_5.png</option>
            <option>level_6.png</option>
            <option>level_7.png</option>
        </property_options>
        <property_options name="LEVEL_FILES">
            <option>level1.sok</option>
            <option>level2.sok</option>
            <option>level3.sok</option>
            <option>level4.sok</option>
            <option>level5.sok</option>
            <option>level6.sok</option>
            <option>level7.sok</option>
        </property_options>
    </property_options_list>
</properties>
\end{lstlisting}

\newpage
\section{Supporting Information}
Note that this document should serve as a reference for those implementing the code, so we’ll provide a table of contents to help quickly find important sections.
\subsection{Table of Contents}
\tableofcontents
\subsection{Appendixes}
N/A








\end{document}