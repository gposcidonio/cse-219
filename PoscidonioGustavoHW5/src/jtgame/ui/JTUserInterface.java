package jtgame.ui;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.ScrollEvent;
import jtgame.game.JTCity;
import jtgame.game.JTGameData;
import jtgame.game.JTGameStateManager;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * This class creates and controls the UI for the entire JTE Application.
 */
public class JTUserInterface extends Pane{
    //This class will hold an instance of itself to maintain itself as a singleton for ease of programming.
    private static JTUserInterface singleton;

    //The top level pane inside which all other panes will be held
    private Stage primaryStage;
    private StackPane mainPane;

    //Video Background Elements
    private Pane videoPane;
    private MediaView videoBackground;
    private File splashScreenVideoFile;
    private Media splashScreenVideo;
    private MediaPlayer splashScreenMediaPlayer;


    //Splash Screen Elements
    private Pane splashScreenPane;
    private VBox splashScreenSelectionBox;
    private Button startButton;
    private Button loadButton;
    private Button aboutSplashButton;
    private Button quitButton;



    //Game Setup Screen Elements
    private BorderPane gameSetupPane;
    private Pane playerSelectionPane;
    private FlowPane northSetupBar;
    private Label numPlayerLabel;
    private ComboBox numPlayersComboBox;
    private Button goButton;
    private ArrayList<ImageView> playerFlagImages;
    private ArrayList<ToggleGroup> playerSelectRadioButtonGroups;
    private ArrayList<RadioButton> playerSelectRadioButtons;
    private Label playerSelectNameLabel;
    private ArrayList<TextField> playerSelectNameTextFields;

    // In-Game Elements
    private BorderPane gamePlayPane;
    private ScrollPane gamePlayScrollPane;
    private Button gameHistoryButton;
    private Button aboutGameButton;
    //    private Button[] quadrantSelectionButtons; <-- I was going to include this but I'm going to try to implement a single page jtgame.game first.
    private JTGameCanvas gameCanvas;
    private Label playerNameLabel;
    private Button saveButton;
    private JTEventHandler eventHandler;
    private ImageView gameMap;
    private DoubleProperty scaleProperty;
    private Pane mapPane;
    private ArrayList<Button> cityNodes;

    //Game History Elements
    private Pane gameHistoryPane;

    //About Pane Elements
//    private Pane aboutPane; <-- I'm going to implement it so that when someone clicks the about button, it just opens the wiki page in their browser.

    /**
     * The default, and only, constructor for the JTUserInterface class. This will call all the initialization methods
     * necessary for the program run and render correctly.
     */
    private JTUserInterface(){
        mainPane = new StackPane();
        eventHandler = new JTEventHandler();
        initVideoPane();
        initSplashScreen();
        initGameSetupScreen();
        initGameScreen();
        initGameHistoryPane();
        changeUIPane(JTGameStateManager.JTScreenState.SPLASH_SCREEN);
    }

    public static JTUserInterface getInstance(){
        if (singleton == null)
            singleton = new JTUserInterface();
        return singleton;
    }

    public void initVideoPane(){
        videoPane = new Pane();
        /*
        Create and play the video file for the background
        TODO: Make this reference something in a file so you could theoretically replace the video with another 16:9 video
         */
        splashScreenVideoFile = new File("media/CloudsAndGrassH264.mp4");
        splashScreenVideo = new Media(splashScreenVideoFile.toURI().toString());
        splashScreenMediaPlayer = new MediaPlayer(splashScreenVideo);
        videoBackground = new MediaView(splashScreenMediaPlayer);
        splashScreenMediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
        splashScreenMediaPlayer.setAutoPlay(true);

        videoPane.getChildren().add(videoBackground);
        mainPane.getChildren().add(videoPane);
    }

    /**
     * Initializes the Splash Screen and all of its elements.
     */
    public void initSplashScreen(){
        /*Create the pane*/
        splashScreenPane = new Pane();



        /*
        Create the box that contains the options for starting a newgame and assin it a correct CSS class. Additionally
        make all the items inside the box align to the center of the box.
         */
        splashScreenSelectionBox = new VBox();
        splashScreenSelectionBox.getStyleClass().add("vbox");
        splashScreenSelectionBox.setAlignment(Pos.CENTER);

        /*Create all the buttons for the splash screen.*/
        startButton = new Button("Start");
        loadButton = new Button("Load");
        aboutSplashButton = new Button("About");
        quitButton = new Button("Quit");


        /*Assign actions to all the different buttons.*/
        quitButton.setOnAction((ActionEvent e) -> System.exit(0));

        startButton.setOnAction((ActionEvent e) -> {
            changeUIPane(JTGameStateManager.JTScreenState.SETUP_SCREEN);
        });

        aboutSplashButton.setOnAction((ActionEvent e) -> {
            if(Desktop.isDesktopSupported()){
                try {
                    Desktop.getDesktop().browse(new URI("http://en.wikipedia.org/wiki/Journey_Through_Europe"));
                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (URISyntaxException e1) {
                    e1.printStackTrace();
                }
            }
        });

        /*Set the CSS ID for all the buttons*/
        startButton.setId("rich-blue");
        loadButton.setId("rich-blue");
        aboutSplashButton.setId("rich-blue");
        quitButton.setId("rich-blue");

        /*Add everything to its appropriate parent in the correct order so that things render correctly.*/
        splashScreenSelectionBox.getChildren().addAll(startButton,loadButton,aboutSplashButton,quitButton);
        splashScreenPane.getChildren().add(splashScreenSelectionBox);
        mainPane.getChildren().add(splashScreenPane);
    }

    /**
     * Initializes the Gameplay Screen and all of tis elements.
     */
    public void initGameScreen(){
        /*TODO:Annotate this block of code with comments.*/
        cityNodes = new ArrayList();
        gamePlayPane = new BorderPane();
        gamePlayScrollPane = new ScrollPane();
        gameMap = new ImageView(new File("artwork/gameplay_AC14.jpg").toURI().toString());

        //------------------------------------------------------------------------------------------------
        /*
        scaleProperty = new SimpleDoubleProperty(1);
        scaleProperty.addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                gameMap.setFitWidth(scaleProperty.get() * 3904);
                gameMap.setFitHeight(scaleProperty.get() * 5144);
                for (int i = 0; i < cityNodes.size(); i++) {
//                    cityNodes.get(i).setMinWidth(scaleProperty.get() * 40);
//                    cityNodes.get(i).setMinHeight(scaleProperty.get() * 40);
//                    cityNodes.get(i).setMaxWidth(scaleProperty.get() * 40);
//                    cityNodes.get(i).setMaxHeight(scaleProperty.get() * 40);
                    cityNodes.get(i).setScaleX(scaleProperty.get());
                    cityNodes.get(i).setScaleY(scaleProperty.get());

                }
            }
        });

        gamePlayScrollPane.setPannable(true);
        gamePlayScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        gamePlayScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        gamePlayScrollPane.addEventFilter(ScrollEvent.ANY, new EventHandler<ScrollEvent>() {
            @Override
            public void handle(ScrollEvent event) {
                if (event.getDeltaY() > 0) {
                    scaleProperty.set(scaleProperty.get() * 1.1);
                    for (int i = 0; i < cityNodes.size(); i++) {
                        double deltaX = (cityNodes.get(i).getImage().getWidth()*scaleProperty.get() - cityNodes.get(i).getImage().getWidth())/2;
                        cityNodes.get(i).setLayoutX(cityNodes.get(i).getLayoutX() - deltaX);
                    }
                } else if (event.getDeltaY() < 0) {
                    if(gamePlayScrollPane.getVvalue() > gamePlayScrollPane.getVmin())

                        if (gameMap.getFitHeight() / 1.1 >= gamePlayScrollPane.getHeight() && gameMap.getFitWidth() / 1.1 >= gamePlayScrollPane.getWidth()) {

                            scaleProperty.set(scaleProperty.get() / 1.1);
                            for (int i = 0; i < cityNodes.size(); i++) {
                                double deltaX = (cityNodes.get(i).getImage().getWidth()/scaleProperty.get() - cityNodes.get(i).getImage().getWidth())/2;
                                cityNodes.get(i).setLayoutX(cityNodes.get(i).getLayoutX() - deltaX);
                            }
                        } else {
                            gameMap.setFitWidth(1600);
                        }

                }
            }
        });
        */
        //------------------------------------------------------------------------------------------------
        gamePlayScrollPane.setPannable(true);
        gamePlayScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        gamePlayScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        gameMap.preserveRatioProperty().set(true);
        gameMap.setFitWidth(1600);
        mapPane = new Pane();

        mapPane.getChildren().add(gameMap);
        gamePlayScrollPane.setContent(mapPane);
        gamePlayPane.setCenter(gamePlayScrollPane);

        VBox box = new VBox();
        Button gameHistoryButton = new Button("Game History");
        gameHistoryButton.setId("rich-blue");
        gameHistoryButton.setOnAction((ActionEvent e)->{
            changeUIPane(JTGameStateManager.JTScreenState.HISTORY_SCREEN);
        });
        box.getChildren().addAll(gameHistoryButton, aboutSplashButton);
        gamePlayPane.setRight(box);

        mainPane.getChildren().add(gamePlayPane);
    }

    /**
     * Initializes the Game Setup Screen and all of its elements.
     */
    public void initGameSetupScreen(){
        System.out.println("From jtgame.ui: "+singleton);
        /*Initialize jtgame.game*/
        gameSetupPane = new BorderPane();
        playerSelectionPane = new Pane();
        northSetupBar = new FlowPane();
        numPlayerLabel = new Label("Input number of players: ");
        numPlayersComboBox = new ComboBox();
        goButton = new Button("Go!");
        playerFlagImages = new ArrayList<>();
        playerSelectRadioButtons = new ArrayList<>();
        playerSelectRadioButtonGroups = new ArrayList<>();
        playerSelectNameTextFields = new ArrayList<>();

        numPlayerLabel.setId("default-label");

        /*Add the different possible player scenarios to the combo box.*/
        numPlayersComboBox.getItems().addAll("2 players","3 players","4 players","5 players","6 players");

        /*Send the number of players to the JTEventHandler to be handled.*/
        numPlayersComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                JTEventHandler.respondToNumPlayersSelected(
                        numPlayersComboBox.getSelectionModel().getSelectedItem().toString()
                );
            }
        });

        /*Add an action listener to the Go! button*/
        goButton.setOnAction((ActionEvent e) -> {
            JTEventHandler.respondToGoButtonPressed();
        });

        /*Setup the north tool bar, which contains a combo box for the number of players and a Go! button for commencing
        the game.*/
        northSetupBar.getChildren().addAll(numPlayerLabel, numPlayersComboBox, goButton);
        northSetupBar.setAlignment(Pos.CENTER);

        /*Add everything to the gameSetupPane and */
        gameSetupPane.setCenter(playerSelectionPane);
        gameSetupPane.setTop(northSetupBar);
        gameSetupPane.setId("jtgame.game-setup-screen");
        mainPane.getChildren().add(gameSetupPane);
    }

    public void initGameHistoryPane(){
        gameHistoryPane = new Pane();
        Button backButton = new Button("Return to game.");
        backButton.setId("rich-blue");
        backButton.setOnAction((ActionEvent e) ->{
            changeUIPane(JTGameStateManager.JTScreenState.GAME_SCREEN);
        });
        gameHistoryPane.getChildren().add(backButton);
        mainPane.getChildren().add(gameHistoryPane);
    }


    /**
     * Plays the initial card distribution animation.
     */
    public void runCardAnimation(){

    }

    /**
     * Displays the map used to select a city to fly to.
     */
    public void displayFlightMap(){

    }

    /**
     * This method is only ever called by the switchScreen method in JTGameStateManager. It switches to a screen
     * depending on the parameter.
     * @param uistate This holds an enum value that determines which screen to switch to.
     */
    public void changeUIPane(JTGameStateManager.JTScreenState uistate){
        /*
        The way this code works is it sets the things you want to see to visible and the things you don't want to see to
        not visible. In the case of the SPLASH_SCREEN and the SETUP_SCREEN you also want to see a video playing in the
        background so we set that to visible then we call the toFront() method on the video pane as well as the
        pane we're working with so that we see the video but interact with the pane that is on top.
         */
        switch(uistate){
            case SPLASH_SCREEN:
                gamePlayPane.setVisible(false);
                gameSetupPane.setVisible(false);
                gameHistoryPane.setVisible(false);
                videoPane.setVisible(true);
                videoPane.toFront();
                splashScreenPane.setVisible(true);
                splashScreenPane.toFront();
                break;
            case GAME_SCREEN:
                gameSetupPane.setVisible(false);
                gameHistoryPane.setVisible(false);
                videoPane.setVisible(false);
                splashScreenPane.setVisible(false);
                gamePlayPane.setVisible(true);
                break;
            case ABOUT_SCREEN:
                break;
            case HISTORY_SCREEN:
                gamePlayPane.setVisible(false);
                gameSetupPane.setVisible(false);
                splashScreenPane.setVisible(false);
                videoPane.setVisible(false);
                gameHistoryPane.setVisible(true);
                break;
            case SETUP_SCREEN:
                gamePlayPane.setVisible(false);
                gameHistoryPane.setVisible(false);
                splashScreenPane.setVisible(false);
                videoPane.setVisible(true);
                videoPane.toFront();
                gameSetupPane.setVisible(true);
                gameSetupPane.toFront();
                break;
        }
    }


//    public void changQuadrant(){
//
//    }


    public MediaView getSplashBackground() {
        return videoBackground;
    }

    public void setSplashBackground(MediaView videoBackground) {
        this.videoBackground = videoBackground;
    }

    public File getSplashScreenVideoFile() {
        return splashScreenVideoFile;
    }

    public void setSplashScreenVideoFile(File splashScreenVideoFile) {
        this.splashScreenVideoFile = splashScreenVideoFile;
    }

    public Media getSplashScreenVideo() {
        return splashScreenVideo;
    }

    public void setSplashScreenVideo(Media splashScreenVideo) {
        this.splashScreenVideo = splashScreenVideo;
    }

    public MediaPlayer getSplashScreenMediaPlayer() {
        return splashScreenMediaPlayer;
    }

    public void setSplashScreenMediaPlayer(MediaPlayer splashScreenMediaPlayer) {
        this.splashScreenMediaPlayer = splashScreenMediaPlayer;
    }

    public Stage getPrimaryStage() { return primaryStage; }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public StackPane getMainPane() {
        return mainPane;
    }

    public Pane getSplashScreenPane() {
        return splashScreenPane;
    }

    public VBox getSplashScreenSelectionBox() {
        return splashScreenSelectionBox;
    }

    public Button getStartButton() {
        return startButton;
    }

    public Button getLoadButton() {
        return loadButton;
    }

    public Button getAboutSplashButton() {
        return aboutSplashButton;
    }

    public Button getQuitButton() {
        return quitButton;
    }

    public BorderPane getGameSetupPane() {
        return gameSetupPane;
    }

    public Pane getPlayerSelectionPane() {
        return playerSelectionPane;
    }

    public Label getNumPlayerLabel() {
        return numPlayerLabel;
    }

    public ComboBox getNumPlayersComboBox() {
        return numPlayersComboBox;
    }

    public Button getGoButton() {
        return goButton;
    }

    public ArrayList<ImageView> getPlayerFlagImages() {
        return playerFlagImages;
    }

    public ArrayList<ToggleGroup> getPlayerSelectRadioButtonGroups() {
        return playerSelectRadioButtonGroups;
    }

    public ArrayList<RadioButton> getPlayerSelectRadioButtons() {
        return playerSelectRadioButtons;
    }

    public Label getPlayerSelectNameLabel() {
        return playerSelectNameLabel;
    }

    public ArrayList<TextField> getPlayerSelectNameTextFields() {
        return playerSelectNameTextFields;
    }

    public ScrollPane getGamePlayPane() {
        return gamePlayScrollPane;
    }

    public Button getGameHistoryButton() {
        return gameHistoryButton;
    }

    public Button getAboutGameButton() {
        return aboutGameButton;
    }

    public JTGameCanvas getGameCanvas() {
        return gameCanvas;
    }

    public Label getPlayerNameLabel() {
        return playerNameLabel;
    }

    public Button getSaveButton() {
        return saveButton;
    }

    public JTEventHandler getEventHandler() {
        return eventHandler;
    }

    public Pane getGameHistoryPane() {
        return gameHistoryPane;
    }

    public Pane getVideoPane() {
        return videoPane;
    }

    public MediaView getVideoBackground() {
        return videoBackground;
    }

    public FlowPane getNorthSetupBar() {
        return northSetupBar;
    }

    public ImageView getGameMap() {
        return gameMap;
    }

    public double getScaleProperty() {
        return scaleProperty.get();
    }

    public DoubleProperty scalePropertyProperty() {
        return scaleProperty;
    }

    public Pane getMapPane() {
        return mapPane;
    }

    public static JTUserInterface getSingleton() {
        return singleton;
    }

    public ArrayList<Button> getCityNodes() {
        return cityNodes;
    }
}
