package jtgame.game;

import javafx.scene.image.Image;

/**
 * This class represents a physical card from the JTE Board Game.
 *
 */
public class JTCard {
    private JTCity associatedCity;
    private JTCardModifier modifier;
    private boolean hasModifier;
    private String color;
    private Image frontSideImg;
    private Image backSideImg;

    public JTCard(JTCity associatedCity, JTCardModifier modifier, boolean hasModifier, String color, Image frontSideImg, Image backSideImg)
    {
        this.associatedCity = associatedCity;
        this.modifier = modifier;
        this.hasModifier = hasModifier;
        this.color = color;
        this.frontSideImg = frontSideImg;
        this.backSideImg = backSideImg;
    }

    public JTCity getAssociatedCity() {
        return associatedCity;
    }

    public JTCardModifier getModifier() {
        return modifier;
    }

    public boolean isHasModifier() {
        return hasModifier;
    }

    public String getColor() {
        return color;
    }

    public Image getFrontSideImg() {
        return frontSideImg;
    }

    public Image getBackSideImg() {
        return backSideImg;
    }
}
