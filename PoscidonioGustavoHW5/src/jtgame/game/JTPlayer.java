package jtgame.game;

import java.util.ArrayList;

/**
 * Created by Freya on 10/31/14.
 */
public class JTPlayer {
    protected String color;
    protected String homeCityName;
    protected int numCards;
    protected ArrayList<String> cardsInHand;
    protected String currentCityName;
    protected boolean hasWonGame;
    protected double currentXCoord;
    protected double currentYCoord;

    public JTPlayer(String color, String homeCityName, int numCards, ArrayList<String> cardsInHand, String currentCityName, boolean hasWonGame, double currentXCoord, double currentYCoord) {
        this.color = color;
        this.homeCityName = homeCityName;
        this.numCards = numCards;
        this.cardsInHand = cardsInHand;
        this.currentCityName = currentCityName;
        this.hasWonGame = hasWonGame;
        this.currentXCoord = currentXCoord;
        this.currentYCoord = currentYCoord;
    }


    //GETTERS AND SETTERS

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getHomeCityName() {
        return homeCityName;
    }

    public void setHomeCityName(String homeCityName) {
        this.homeCityName = homeCityName;
    }

    public int getNumCards() {
        return numCards;
    }

    public void setNumCards(int numCards) {
        this.numCards = numCards;
    }

    public ArrayList<String> getCardsInHand() {
        return cardsInHand;
    }

    public void setCardsInHand(ArrayList<String> cardsInHand) {
        this.cardsInHand = cardsInHand;
    }

    public String getCurrentCityName() {
        return currentCityName;
    }

    public void setCurrentCityName(String currentCityName) {
        this.currentCityName = currentCityName;
    }

    public boolean isHasWonGame() {
        return hasWonGame;
    }

    public void setHasWonGame(boolean hasWonGame) {
        this.hasWonGame = hasWonGame;
    }

    public double getCurrentXCoord() {
        return currentXCoord;
    }

    public void setCurrentXCoord(double currentXCoord) {
        this.currentXCoord = currentXCoord;
    }

    public double getCurrentYCoord() {
        return currentYCoord;
    }

    public void setCurrentYCoord(double currentYCoord) {
        this.currentYCoord = currentYCoord;
    }
}
