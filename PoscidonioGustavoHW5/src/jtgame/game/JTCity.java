package jtgame.game;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Freya on 10/31/14.
 */
public class JTCity {
    private String name;
    private String flavorText;
    private boolean hasHardbor;
    private boolean hasAirport;
    private double xCoordinate;
    private double yCoordinate;
    private double xFlightCoordinates;
    private double yFlightCoordinates;
    private ArrayList<String> landAdjacancies;
    private ArrayList<String> hardborAdjacancies;
    private HashMap<String,Integer> airAdjacencies;

    public JTCity(String name, String flavorText, boolean hasHardbor, boolean hasAirport, double xCoordinate,
                  double yCoordinate, double xFlightCoordinates, double yFlightCoordinates,
                  ArrayList<String> landAdjacancies, ArrayList<String> hardborAdjacancies, HashMap<String,
                  Integer> airAdjacencies) {
        this.name = name;
        this.flavorText = flavorText;
        this.hasHardbor = hasHardbor;
        this.hasAirport = hasAirport;
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.xFlightCoordinates = xFlightCoordinates;
        this.yFlightCoordinates = yFlightCoordinates;
        this.landAdjacancies = landAdjacancies;
        this.hardborAdjacancies = hardborAdjacancies;
        this.airAdjacencies = airAdjacencies;
    }

    /**
     * Checks if the city in question is adjacent to this city.
     * @param cityName The name of the potentially adjacent city.
     * @return Returns 1 if adjacent by land, 2 or 4 if adjacent by air, 1000 if adjacent by harbor and -1 if
     * not adjacent.
     */
    public int checkIfAdjacent(String cityName)
    {
        //TODO
        return 1;
    }

    //GETTER METHODS

    public String getName() {
        return name;
    }

    public String getFlavorText() {
        return flavorText;
    }

    public boolean isHasHardbor() {
        return hasHardbor;
    }

    public boolean isHasAirport() {
        return hasAirport;
    }

    public double getxCoordinate() {
        return xCoordinate;
    }

    public double getyCoordinate() {
        return yCoordinate;
    }

    public double getxFlightCoordinates() {
        return xFlightCoordinates;
    }

    public double getyFlightCoordinates() {
        return yFlightCoordinates;
    }

    public ArrayList<String> getLandAdjacancies() {
        return landAdjacancies;
    }

    public ArrayList<String> getHardborAdjacancies() {
        return hardborAdjacancies;
    }

    public HashMap<String, Integer> getAirAdjacencies() {
        return airAdjacencies;
    }




















}
