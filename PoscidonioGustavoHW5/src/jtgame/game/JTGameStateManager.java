package jtgame.game;

import jtgame.ui.JTUserInterface;

/**
 * Created by Freya on 10/31/14.
 */
public class JTGameStateManager {

    /**
     * An enumerated type indicating the current screen in the JTE Game.
     */
    public enum JTScreenState{
        GAME_SCREEN, SETUP_SCREEN, ABOUT_SCREEN, HISTORY_SCREEN, SPLASH_SCREEN;
    }

    /**
     * An enumerated type indicating whether the current jtgame.game is a newly created jtgame.game, a jtgame.game in progress or
     * a jtgame.game over.
     */
    public enum JTGameState{
        NEW_GAME, GAME_IN_PROGRESS, GAME_OVER;
    }

    /*
    The JTGameStateManager (GSM) holds a reference to all the elements of the jtgame.game and, well, manages them. All details about
    a jtgame.game are held in JTGameData and all UI elements are in JTUserInterface. The GSM also holds a value  for the
     */
    public JTGameData currentGameData;
    public JTUserInterface ui;
    public JTGameState gameState;
    public JTScreenState screenState;

    /**
     * Calls the initGUI method and switchScreen method and places the user on the splash screen.
     */
    public JTGameStateManager() {
        initGUI();
        switchScreen(JTScreenState.SPLASH_SCREEN);
    }



    /**
     * Initializes the gameData to a default starting point based on the number of players in the jtgame.game.
     * Additionally it switches to the Gameplay screen, deals the starting cards and starts the first player's turn by
     * rolling the die for them.
     */
    public void startNewGame(){
        initGameData();
        switchScreen(JTScreenState.GAME_SCREEN);
        dealCards();
        startTurn();
    }

    /**
     * Initializes the JTUserInterface object and all of its components, preparing them for use
     * in the jtgame.game.
     */
    public void initGUI(){
        ui = JTUserInterface.getInstance();
    }

    /**
     * Pulls information from the GameSetup screen to initialize the JTGameData object with default values.
     */
    public void initGameData(){
        //TODO
    }

    /**
     * This is the top-level method for processing a move request. It is passed a String that is the name of the city
     * you want to travel to and determines whether or not you're allowed to do that move.
     * @param cityName
     */
    public void processMoveRequest(String cityName){
        //TODO
    }

    /**
     * Setter method for the jtgame.game's current state.
     * @param gameState
     */
    public void setGameState(JTGameState gameState) {
        this.gameState = gameState;
    }

    /**
     * Tell's the JTUserInterface to change the screen that's visible.
     * @param screenState
     */
    public void switchScreen(JTScreenState screenState){
        this.screenState = screenState;
        ui.changeUIPane(screenState);
    }

    /**
     * Quits the jtgame.game.
     */
    public void processQuit(){
        System.exit(0);
    }

    /**
     * Reads in the information from a save file and resets the jtgame.game to where the last saved jtgame.game was.
     */
    public void loadGame(){
        //TODO
    }

    /**
     * Tells the user interface to render itself.
     */
    public void updateUI(){
        //TODO
    }

    /**
     * Starts the turn for the current player.
     */
    public void startTurn(){
        //TODO
    }

    /**
     * Ends the turn for the current player.
     */
    public void endTurn(){
        //TODO
    }

    /**
     * Check if the current player has won.
     * @return Whether the current player won the jtgame.game.
     */
    public boolean checkIfPlayerWon(){
        //TODO
        return false;
    }

    /**
     * Performs all the housekeeping for a jtgame.game over.
     */
    public void endGame(){
        //TODO
    }

    /**
     * Deals the cards to each player and runs the JTUserInterface runCardAnimation method which plays the animation for
     * dealing cards.
     */
    public void dealCards(){
        //TODO
    }

    /**
     * Checks if air travel is possible based on the number of moves you have left and performs the move if possible.
     * @return Whether or not the move happened.
     */
    public boolean tryAirTravel(){
        //TODO
        return true;
    }

    public JTGameData getCurrentGameData() {
        return currentGameData;
    }

    public void setCurrentGameData(JTGameData currentGameData) {
        this.currentGameData = currentGameData;
    }

    public JTUserInterface getUI() {
        return ui;
    }

    public void setUI(JTUserInterface ui) {
        this.ui = ui;
    }

    public JTGameState getGameState() {
        return gameState;
    }

    public JTScreenState getScreenState() {
        return screenState;
    }

    public void setScreenState(JTScreenState screenState) {
        this.screenState = screenState;
    }
}
