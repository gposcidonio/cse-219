package jtgame.game;

import jtgame.io.JTFileIO;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class serves as a house for the data needed for the jtgame.game. All the data related to the jtgame.game is in this class so
 * a jtgame.game can be completely loaded by simply loading the information in this class from a file.
 */
public class JTGameData {

    private static JTGameData instance = null;
    private static HashMap<String,JTCity> cities;
    private static HashMap<String,JTCard> cards;
    private static JTPlayer[] players;
    private static ArrayDeque<JTPlayer> playerTurns;
    private static ArrayList<String[]> turnHistory;
    private static JTDie die;
    private static double gameWidth;
    private static double gameHeight;
    private static JTFileIO fileIO;

    /**
     * The default constructor for a new JTGameData.
     */
    private JTGameData(){
        //TODO
        initCities();
    }


    public synchronized static JTGameData getInstance(){
        if (instance == null){
            instance = new JTGameData();
        }

        return instance;
    }

    /**
     * A method that reads information from a file and loads the data accordingly.
     */
    public void loadGameData(){
        //TODO
    }

    public static void initCities(){
        cities = new HashMap();
        JTCity reykjavik = new JTCity("Reykjavik","flavor",false,true,200,710,-1,-1,null,null,null);
        cities.put(reykjavik.getName(),reykjavik);
        JTCity faeroeIs = new JTCity("Faeroe Island","flavor",false,false,936,1282,-1,-1,null,null,null);
        cities.put(faeroeIs.getName(),faeroeIs);
        JTCity shetlandIs = new JTCity("Shetland Island","flavor",false,false,1222,1544,-1,-1,null,null,null);
        cities.put(shetlandIs.getName(),shetlandIs);
        JTCity bergen = new JTCity("Bergen","flavor",false,false,1650,1582,-1,-1,null,null,null);
        cities.put(bergen.getName(),bergen);
        JTCity inverness = new JTCity("Inverness","flavor",false,false,955,1877,-1,-1,null,null,null);
        cities.put(inverness.getName(),inverness);
        JTCity aberdeen = new JTCity("Aberdeen","flavor",false,false,1090,1930,-1,-1,null,null,null);
        cities.put(aberdeen.getName(),aberdeen);
        JTCity glasgow = new JTCity("Glasgow","flavor",false,true,895,2072,-1,-1,null,null,null);
        cities.put(glasgow.getName(),glasgow);
        JTCity stavanger = new JTCity("Stavanger","flavor",false,false,1650,1750,-1,-1,null,null,null);
        cities.put(stavanger.getName(),stavanger);
        JTCity aarhus = new JTCity("Aarhus","flavor",false,false,1984,2125,-1,-1,null,null,null);
        cities.put(aarhus.getName(),aarhus);
        JTCity belfast = new JTCity("Belfast","flavor",false,false,763,2191,-1,-1,null,null,null);
        cities.put(belfast.getName(),belfast);
        JTCity dublin = new JTCity("Dublin","flavor",false,true,687,2347,-1,-1,null,null,null);
        cities.put(dublin.getName(),dublin);
        JTCity cork = new JTCity("Cork","flavor",false,false,465,2492,-1,-1,null,null,null);
        cities.put(cork.getName(),cork);
        JTCity liverpool = new JTCity("Liverpool","flavor",false,false,939,2396,-1,-1,null,null,null);
        cities.put(liverpool.getName(),liverpool);
        JTCity leeds = new JTCity("Leeds","flavor",false,false,1044,2354,-1,-1,null,null,null);
        cities.put(leeds.getName(),leeds);
        JTCity birmingham = new JTCity("Birmingham","flavor",false,false,981,2539,-1,-1,null,null,null);
        cities.put(birmingham.getName(),birmingham);
        JTCity groningen = new JTCity("Groningen","flavor",false,false,1656,2488,-1,-1,null,null,null);
        cities.put(groningen.getName(),groningen);
        JTCity hamburg = new JTCity("Hamburg","flavor",false,false,1972,2465,-1,-1,null,null,null);
        cities.put(hamburg.getName(),hamburg);
        JTCity newCastle = new JTCity("Newcastle","flavor",false,false,1063,2215,-1,-1,null,null,null);
        cities.put(newCastle.getName(),newCastle);
    }

    public HashMap<String, JTCity> getCities() {
        return cities;
    }

    public HashMap<String, JTCard> getCards() {
        return cards;
    }

    public JTPlayer[] getPlayers() {
        return players;
    }

    public ArrayDeque<JTPlayer> getPlayerTurns() {
        return playerTurns;
    }

    public ArrayList<String[]> getTurnHistory() {
        return turnHistory;
    }

    public JTDie getDie() {
        return die;
    }

    public double getGameWidth() {
        return gameWidth;
    }

    public double getGameHeight() {
        return gameHeight;
    }

    public JTFileIO getFileIO() {
        return fileIO;
    }

    public void setCities(HashMap<String, JTCity> cities) {
        this.cities = cities;
    }

    public void setCards(HashMap<String, JTCard> cards) {
        this.cards = cards;
    }

    public void setPlayers(JTPlayer[] players) {
        this.players = players;
    }

    public void setPlayerTurns(ArrayDeque<JTPlayer> playerTurns) {
        this.playerTurns = playerTurns;
    }

    public void setTurnHistory(ArrayList<String[]> turnHistory) {
        this.turnHistory = turnHistory;
    }

    public void setDie(JTDie die) {
        this.die = die;
    }

    public void setGameWidth(double gameWidth) {
        this.gameWidth = gameWidth;
    }

    public void setGameHeight(double gameHeight) {
        this.gameHeight = gameHeight;
    }

    public void setFileIO(JTFileIO fileIO) {
        this.fileIO = fileIO;
    }
}
