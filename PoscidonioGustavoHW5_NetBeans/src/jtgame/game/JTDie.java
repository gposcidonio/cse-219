package jtgame.game;

/**
 * The JTDie class represents a multi-sided die that can be rolled to determine the number of moves a JTPlayer can make
 * on a given turn.
 */
public class JTDie {
    private int numSides;
    private int sideFacingUp;

    public JTDie(){
        numSides = 6;
        sideFacingUp = 1;
    }

    public JTDie(int numSides){
        this.numSides = numSides;
        sideFacingUp = 1;
    }

    public int roll(){
        sideFacingUp = (int)(Math.random()*numSides + 1);
        return sideFacingUp;
    }

    public int getNumSides() {
        return numSides;
    }

    public void setNumSides(int numSides) {
        this.numSides = numSides;
    }

    public int getSideFacingUp() {
        return sideFacingUp;
    }

    public void setSideFacingUp(int sideFacingUp) {
        this.sideFacingUp = sideFacingUp;
    }
}
