import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.File;
import java.net.MalformedURLException;

/**
 * Created by Gustavo Poscidonio on 11/22/2014.
 */
public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }
    public void start(Stage primaryStage){
        ScrollPane scrollPane = new ScrollPane();
        Pane mapPane = new Pane();
        Canvas canvas = new Canvas(3904,5144);
        GraphicsContext gc = canvas.getGraphicsContext2D();

        scrollPane.setPannable(true);

        Image gameMap = new Image("file:///S:/Documents/Repositories/CSE 219/FinalProjectCoordinateGetter/src/gameplay.jpg");
        gc.drawImage(gameMap,0,0,3904,5104);
        mapPane.setMaxSize(2880,1800);
        mapPane.getChildren().add(canvas);
        scrollPane.setContent(canvas);
        Scene scene = new Scene(scrollPane, 2880, 1800);
        primaryStage.setScene(scene);
        primaryStage.show();

        canvas.setOnMouseClicked((MouseEvent me) -> {
            if (me.getClickCount() > 1) {
                double xCoordinate = me.getX();
                double yCoordinate = me.getY();
                String s = JOptionPane.showInputDialog("Input city name:");
                System.out.println(s+","+xCoordinate+","+yCoordinate);
            }
        });
    }
}
