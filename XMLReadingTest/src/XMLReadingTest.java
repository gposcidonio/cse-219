import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Gustavo Poscidonio on 11/22/2014.
 */
public class XMLReadingTest {
    public static void main(String[] args) {
        /*
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try{
            builder = builderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException p){
            p.printStackTrace();
        }
        Document document = null;
        try{
            document = builder.parse(new FileInputStream("cities.xml"));
        } catch (SAXException s){
            s.printStackTrace();
        } catch (IOException i){
            i.printStackTrace();
        }

        Element rootElement = document.getDocumentElement();

        NodeList listOfCities = rootElement.getChildNodes();
        for (int i = 0; i < listOfCities.getLength(); i++) {
            Node cityNode = listOfCities.item(i);

            if (cityNode instanceof Element){
                NodeList cityProperties = cityNode.getChildNodes();

                for (int j = 0; j < cityProperties.getLength(); j++) {
                    Node property = cityProperties.item(j);

                    if (property instanceof Element){
                        String content = property.getTextContent();
                        if (property.getNodeName().matches("name") || property.getNodeName().matches("flightSector")) {
                            System.out.println(property.getNodeName() + ": " + content);
                        } else {
                            NodeList adjacency = ((Element) property).getChildNodes();
                            for (int k = 0; k < adjacency.getLength(); k++) {
                                if (adjacency.item(k).hasChildNodes() && adjacency.item(k).getNodeName().matches("land")) {
                                    Node landAdjacency = adjacency.item(k);
                                    NodeList landAdjacencyList = landAdjacency.getChildNodes();
                                    for (int l = 0; l < landAdjacency.; l++) {
                                        Node landNeighborCity = landAdjacencyList.item(l);
                                        if (landNeighborCity instanceof Element){

                                        }
                                    }
                                    System.out.println("Land Adjacency " + k + ":" + adjacentCity.getTextContent());
                                }
                            }
                        }
                    }

                }
            }
        }
        */
        ArrayList<City> cityList = null;
        City currentCity = null;
        String tagContent = null;
        XMLInputFactory factory = XMLInputFactory.newInstance();
        try {
            XMLStreamReader reader = factory.createXMLStreamReader(new FileInputStream(new File("S:/Documents/Repositories/CSE 219/XMLReadingTest/cities.xml")));
            while (reader.hasNext()) {
                int event = reader.next();

                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        if ("cityNode".equals(reader.getLocalName())) {
                            currentCity = new City();
                        }
                        if ("routes".equals(reader.getLocalName())) {
                            cityList = new ArrayList<>();
                        }
                        if ("land".equals(reader.getLocalName())) {
                            currentCity.landCities = new ArrayList<>();
                            while (event != XMLStreamConstants.END_ELEMENT || !reader.getLocalName().matches("land")) {
                                event = reader.next();
                                if(event == XMLStreamConstants.START_ELEMENT && reader.getLocalName().matches("city")) {
                                    event = reader.next();
                                        currentCity.landCities.add(reader.getText().trim());
                                }
                            }
                        }
                        if ("sea".equals(reader.getLocalName())) {
                            currentCity.seaCities = new ArrayList<>();
                            while (event != XMLStreamConstants.END_ELEMENT || !reader.getLocalName().matches("sea")) {
                                event = reader.next();
                                if (event == XMLStreamConstants.START_ELEMENT && reader.getLocalName().matches("city")) {
                                    event = reader.next();
                                    currentCity.seaCities.add(reader.getText().trim());
                                }
                            }
                        }
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        tagContent = reader.getText().trim();
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        switch (reader.getLocalName()) {
                            case "name":
                                currentCity.name = tagContent;
                                break;
                            case "flightSector":
                                currentCity.flightSector = Integer.parseInt(tagContent);
                                break;
                            case "cityNode":
                                cityList.add(currentCity);
                            default:
                                //do nothing
                                break;
                        }
                        break;
                    case XMLStreamConstants.START_DOCUMENT:
                        cityList = new ArrayList<>();
                    default:
                        //do nothing
                        break;

                }
            }

        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (FileNotFoundException f){
            System.out.println("oh shit");
        }
        for (City c : cityList) {
            System.out.println(c);
        }
    }
}

class City{
    String name;
    int flightSector;
    ArrayList<String> landCities;
    ArrayList<String> seaCities;
    public String toString(){
        StringBuffer buffer = new StringBuffer();
        buffer.append("Name: ").append(name).append("\nFlight Sector: ").append(flightSector).append("\nLand:\n");
        if (landCities != null) {
            for (String s : landCities) {
                buffer.append("\t").append(s).append("\n");
            }
        }
        buffer.append("Sea:\n");
        if(seaCities != null) {
            for (String s : seaCities) {
                buffer.append("\t").append(s).append("\n");
            }
        }
        return buffer.toString();
    }
}
