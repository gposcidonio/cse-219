package HangMan.ui;

import java.io.IOException;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

public class HelpHyperlinkListener implements HyperlinkListener{
	private HangManUI ui;
	
	public HelpHyperlinkListener(HangManUI initUI){
		ui = initUI;
                
	}
        

    @Override
    public void hyperlinkUpdate(HyperlinkEvent he) {
        if (he.getEventType() == HyperlinkEvent.EventType.ACTIVATED){
            try{
                java.awt.Desktop.getDesktop().browse(java.net.URI.create("http://en.wikipedia.org/wiki/Hangman_(game)"));
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
