package application;


import jtgame.game.JTGameStateManager;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import jtgame.ui.JTUserInterface;
//import properties_manager.PropertiesManager;

import java.io.File;

/**
 * This is the main driver class for the JTE Application
 */
public class Main extends Application{
    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage){
        try {

//            PropertiesManager props = PropertiesManager.getPropertiesManager();
//            props.addProperty("JTEuropeCityLocations.xml","JTEuropeCityLocations.xml");
//            props.addProperty("JTSchema.xsd","JTSchema.xsd");
//            props.addProperty("S:/Documents/Repositories/CSE 219/PoscidonioGustavoHW5/game_data","S:/Documents/Repositories/CSE 219/PoscidonioGustavoHW5/game_data");


            String title = "Journey Through Europe";

            primaryStage.setTitle(title);

            JTGameStateManager gsm = JTGameStateManager.getInstance();

            JTUserInterface root = JTUserInterface.getInstance();

            StackPane mainPane = root.getMainPane();

            root.setPrimaryStage(primaryStage);


            Scene scene = new Scene(mainPane, mainPane.getWidth(), mainPane.getHeight());

            File f = new File("JTStyleSheet.css");

            String css = Main.class.getResource("/jtgame/style/JTStyleSheet.css").toExternalForm();

            scene.getStylesheets().clear();

            scene.getStylesheets().add(css);

            primaryStage.setScene(scene);

            primaryStage.setHeight(929);
            primaryStage.setWidth(1600);
            primaryStage.setResizable(false);

            primaryStage.show();


            System.out.println("primaryStage height= " + primaryStage.getHeight());
            System.out.println("primaryStage width = " + primaryStage.getWidth());
            root.getSplashScreenSelectionBox().setLayoutX(primaryStage.getWidth()/2.0);
            root.getSplashScreenSelectionBox().setLayoutY(primaryStage.getHeight()/2.0+50);
            root.getSplashBackground().setFitWidth(1600);
            root.getSplashBackground().setFitHeight(900);

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public enum JTProperties{

    }
}
