package jtgame.game;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Freya on 10/31/14.
 */
public class JTCity {
    private String name;
    private int flightSector;
    private String color;
    private double xCoordinate;
    private double yCoordinate;
    private ArrayList<String> landAdjacancies;
    private ArrayList<String> harborAdjacancies;
    private HashMap<String, Integer> airAdjacencies;

    public JTCity() {
        landAdjacancies = new ArrayList<>();
        harborAdjacancies = new ArrayList<>();
        airAdjacencies = new HashMap<>();
    }

    public JTCity(String name, boolean hasHarbor, boolean hasAirport, int flightSector, double xCoordinate,
                  double yCoordinate, double xFlightCoordinates, double yFlightCoordinates,
                  ArrayList<String> landAdjacancies, ArrayList<String> harborAdjacancies, HashMap<String,
            Integer> airAdjacencies) {
        this.name = name;

        this.flightSector = flightSector;
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.landAdjacancies = landAdjacancies;
        this.harborAdjacancies = harborAdjacancies;
        this.airAdjacencies = airAdjacencies;
    }

    /**
     * Checks if the city in question is adjacent to this city.
     *
     * @param cityName The name of the potentially adjacent city.
     * @return Returns 1 if adjacent by land, 2 or 4 if adjacent by air, 1000 if adjacent by harbor and -1 if
     * not adjacent.
     */
    public int checkIfAdjacent(String cityName) {
        //TODO
        return 1;
    }

    //GETTER METHODS

    public String getName() {
        return name;
    }


    public double getxCoordinate() {
        return xCoordinate;
    }

    public double getyCoordinate() {
        return yCoordinate;
    }

    public ArrayList<String> getLandAdjacancies() {
        return landAdjacancies;
    }

    public ArrayList<String> getHarborAdjacancies() {
        return harborAdjacancies;
    }

    public HashMap<String, Integer> getAirAdjacencies() {
        return airAdjacencies;
    }

    public int getFlightSector() {
        return flightSector;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFlightSector(int flightSector) {
        this.flightSector = flightSector;
    }

    public void setxCoordinate(double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public void setyCoordinate(double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public void setLandAdjacancies(ArrayList<String> landAdjacancies) {
        this.landAdjacancies = landAdjacancies;
    }

    public void setHarborAdjacancies(ArrayList<String> harborAdjacancies) {
        this.harborAdjacancies = harborAdjacancies;
    }

    public void setAirAdjacencies(HashMap<String, Integer> airAdjacencies) {
        this.airAdjacencies = airAdjacencies;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


}
