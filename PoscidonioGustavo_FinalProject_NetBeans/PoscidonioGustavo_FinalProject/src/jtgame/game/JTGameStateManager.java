package jtgame.game;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import jtgame.ui.JTEventHandler;
import jtgame.ui.JTUserInterface;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Freya on 10/31/14.
 */
public class JTGameStateManager {

    /*
    The JTGameStateManager (GSM) holds a reference to all the elements of the jtgame.game and, well, manages them. All details about
    a jtgame.game are held in JTGameData and all UI elements are in JTUserInterface. The GSM also holds a value  for the
     */
    private JTUserInterface ui;
    private JTGameState gameState;
    private JTScreenState screenState;
    private static JTGameStateManager singleton;

    private int turnCounter;

    private static boolean gameOver = false;

    /**
     * An enumerated type indicating the current screen in the JTE Game.
     */
    public enum JTScreenState{
        GAME_SCREEN, SETUP_SCREEN, ABOUT_SCREEN, HISTORY_SCREEN, SPLASH_SCREEN,CITY_DESCRIPTIONS;
    }

    /**
     * An enumerated type indicating whether the current jtgame.game is a newly created jtgame.game, a jtgame.game in progress or
     * a jtgame.game over.
     */
    public enum JTGameState{
        NEW_GAME, GAME_IN_PROGRESS, GAME_OVER;
    }



    /**
     * Calls the initGUI method and switchScreen method and places the user on the splash screen.
     */
    private JTGameStateManager() {
        turnCounter = 0;
        initGUI();
        initGameResources();
        switchScreen(JTScreenState.SPLASH_SCREEN);
    }

    public synchronized static JTGameStateManager getInstance(){
        if (singleton == null){
            singleton = new JTGameStateManager();
        }
        return singleton;
    }







    /**
     * Initializes the gameData to a default starting point based on the number of players in the jtgame.game.
     * Additionally it switches to the Gameplay screen, deals the starting cards and starts the first player's turn by
     * rolling the die for them.
     */
    public void startNewGame(){
        switchScreen(JTScreenState.GAME_SCREEN);
        dealCards();
        startTurn();
    }

    /**
     * Initializes the JTUserInterface object and all of its components, preparing them for use
     * in the jtgame.game.
     */
    public void initGUI(){
        ui = JTUserInterface.getInstance();
    }

    /**
     * Pulls information from the GameSetup screen to initialize the JTGameData object with default values.
     */
    public void initGameResources(){
        JTGameResources res = JTGameResources.getInstance();
        res.initCities();
        res.initCards();
    }

    public void enableMoving(){
        ui = JTUserInterface.getInstance();
        ui.getGameCanvas().setOnMouseClicked((javafx.scene.input.MouseEvent me) -> {
            JTGameResources res = JTGameResources.getInstance();
            if (res.getPlayers()[turnCounter] instanceof JTHooman)
                JTEventHandler.respondToClickOnMap(me, turnCounter);
        });
        ui.getGameCanvas().clean();
        ui.getGameCanvas().colorAdjacentCities();
    }

    public void disableMoving(){

    }

    /**
     * This is the top-level method for processing a move request. It is passed a String that is the name of the city
     * you want to travel to and determines whether or not you're allowed to do that move.
     * @param cityName
     */
    public void processMoveRequest(String cityName){
        JTGameResources res = JTGameResources.getInstance();
        JTPlayer currentPlayer = res.getPlayers()[turnCounter];
        ui = JTUserInterface.getInstance();
        int moveCost = 0;
        boolean youMayProceed = false;


        for (String adjacentCity : res.getCities().get(currentPlayer.getCurrentCityName()).getLandAdjacancies()){
            if (cityName.matches(adjacentCity)){
                youMayProceed = true;
                moveCost = 1;
                break;
            }
        }

        if (!youMayProceed && !currentPlayer.hasMoved()){
            for (String adjacentCity : res.getCities().get(currentPlayer.getCurrentCityName()).getHarborAdjacancies()){
                if (cityName.matches(adjacentCity)){
                    youMayProceed = true;
                    moveCost = 6;
                    break;
                }
            }
        }

        if(!youMayProceed){
            for (String adjacentCity : res.getCities().get(currentPlayer.getCurrentCityName()).getAirAdjacencies().keySet()){
                if(cityName.matches(adjacentCity)){
                    youMayProceed = true;
                    moveCost = res.getCities().get(currentPlayer.getCurrentCityName()).getAirAdjacencies().get(adjacentCity);
                    if (moveCost > currentPlayer.getRemainingMoves())
                        youMayProceed = false;
                    break;
                }
            }
        }

        for (JTPlayer player : res.getPlayers()){
            if(player.getCurrentCityName().matches(cityName)) {
                youMayProceed = false;
                break;
            }
        }

        if (currentPlayer.getRemainingMoves() <= 0){
            youMayProceed = false;
        }

        if (youMayProceed){
            int remainingMoves = currentPlayer.getRemainingMoves()- moveCost;
            if (remainingMoves < 0)
                remainingMoves = 0;
            currentPlayer.setRemainingMoves(remainingMoves);
            ui.getNumMovesLabel().setText("Moves Remaining: " + currentPlayer.getRemainingMoves());
            ui.addHistoryText(currentPlayer.getPlayerName() + " moved from " + currentPlayer.getCurrentCityName() + " to " + cityName + ".");
            ui.moveCurrentPlayer(cityName);
            currentPlayer.setHasMoved(true);
            currentPlayer.setCurrentCityName(cityName);
//            ui.getGameCanvas().colorAdjacentCities();
            if(currentPlayer.getCardsInHand().contains(currentPlayer.getCurrentCityName())){
                int cardIndex = currentPlayer.getCardsInHand().indexOf(currentPlayer.getCurrentCityName());
                currentPlayer.getCardsInHand().remove(currentPlayer.getCurrentCityName());
                currentPlayer.getCardsInHand().trimToSize();
                ui.runRemoveAnimation(cardIndex);
            }

            if(currentPlayer.getCardsInHand().size() == 0){
                endGame();
            }
        }
    }

    /**
     * Setter method for the jtgame.game's current state.
     * @param gameState
     */
    public void setGameState(JTGameState gameState) {
        this.gameState = gameState;
    }

    /**
     * Tell's the JTUserInterface to change the screen that's visible.
     * @param screenState
     */
    public void switchScreen(JTScreenState screenState){
        this.screenState = screenState;
        ui.changeUIPane(screenState);
    }

    /**
     * Quits the jtgame.game.
     */
    public void processQuit(){
        System.exit(0);
    }

    /**
     * Reads in the information from a save file and resets the jtgame.game to where the last saved jtgame.game was.
     */
    public void loadGame(){
        //TODO
    }

    /**
     * Tells the user interface to render itself.
     */
    public void updateUI(){
        //TODO
    }

    /**
     * Starts the turn for the current player.
     */
    public void startTurn(){
        ui = JTUserInterface.getInstance();
        JTGameResources res = JTGameResources.getInstance();
        ui.getLeftPane().getChildren().clear();
        ui.setLeftPaneForCurrentPlayer(turnCounter);
        ui.centerOnCurrentPlayer(turnCounter);
        res.getPlayers()[turnCounter].setHasMoved(false);
        if(res.getPlayers()[turnCounter] instanceof JTHooman) {
            ui.getDieRoll().setDisable(false);
        } else {
            if (!gameOver)
            res.getPlayers()[turnCounter].executeTurn();
        }
    }

    /**
     * Proceeds to the next turn.
     */
    public void nextTurn(){

    }

    /**
     * Ends the turn for the current player.
     */
    public void endTurn(){
        JTGameResources res = JTGameResources.getInstance();
        turnCounter = (++turnCounter)%res.getPlayers().length;
        startTurn();
    }

    /**
     * Check if the current player has won.
     * @return Whether the current player won the jtgame.game.
     */
    public boolean checkIfPlayerWon(){
        //TODO
        return false;
    }




    /**
     * Deals the cards to each player and runs the JTUserInterface runCardAnimation method which plays the animation for
     * dealing cards.
     */
    public void dealCards(){
        //TODO
    }

    /**
     * Checks if air travel is possible based on the number of moves you have left and performs the move if possible.
     * @return Whether or not the move happened.
     */
    public boolean tryAirTravel(){
        //TODO
        return true;
    }

    public JTPlayer getCurrentPlayer(){
        JTGameResources res = JTGameResources.getInstance();
        return res.getPlayers()[turnCounter];
    }

    /**
     * Performs all the housekeeping for a jtgame.game over.
     */
    public void endGame(){
        gameOver = true;
        ui = JTUserInterface.getInstance();
        ui.endGame();
    }


    public JTGameState getGameState() {
        return gameState;
    }

    public JTScreenState getScreenState() {
        return screenState;
    }

    public void setScreenState(JTScreenState screenState) {
        this.screenState = screenState;
    }

    public JTUserInterface getUi() {
        return ui;
    }

    public void setUi(JTUserInterface ui) {
        this.ui = ui;
    }

    public int getTurnCounter() {
        return turnCounter;
    }

    public void setTurnCounter(int turnCounter) {
        this.turnCounter = turnCounter;
    }

    public static boolean isGameOver() {
        return gameOver;
    }

    public static void setGameOver(boolean gameOver) {
        JTGameStateManager.gameOver = gameOver;
    }
}
