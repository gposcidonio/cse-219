package jtgame.io;

//import com.sun.java.browser.plugin2.DOM;
import jtgame.game.*;
import jtgame.ui.JTUserInterface;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by Freya on 10/31/14.
 */
public class JTFileIO {
    private static final String SAVE_FILE = "JTSaveFile.sav";
    public static void loadCities(){
        JTGameResources res = JTGameResources.getInstance();
        HashMap<String,JTCity> cities = new HashMap<>();
        JTCity currentCity = null;
        String tagContent = null;
        XMLInputFactory factory = XMLInputFactory.newInstance();
        try {
            XMLStreamReader reader = factory.createXMLStreamReader(new FileInputStream(new File("game_resources/cities.xml")));
            while (reader.hasNext()) {
                int event = reader.next();

                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        if ("cityNode".equals(reader.getLocalName())) {
                            currentCity = new JTCity();
                        }
                        if ("routes".equals(reader.getLocalName())) {
//                            cityList = new ArrayList<>();
                        }
                        if ("land".equals(reader.getLocalName())) {
                            while (event != XMLStreamConstants.END_ELEMENT || !reader.getLocalName().matches("land")) {
                                event = reader.next();
                                if(event == XMLStreamConstants.START_ELEMENT && reader.getLocalName().matches("city")) {
                                    event = reader.next();
                                    currentCity.getLandAdjacancies().add(reader.getText().trim());
                                }
                            }
                        }
                        if ("sea".equals(reader.getLocalName())) {
                            while (event != XMLStreamConstants.END_ELEMENT || !reader.getLocalName().matches("sea")) {
                                event = reader.next();
                                if (event == XMLStreamConstants.START_ELEMENT && reader.getLocalName().matches("city")) {
                                    event = reader.next();
                                    currentCity.getHarborAdjacancies().add(reader.getText().trim());
                                }
                            }
                        }
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        tagContent = reader.getText().trim();
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        switch (reader.getLocalName()) {
                            case "name":
                                currentCity.setName(tagContent);
                                break;
                            case "flightSector":
                                currentCity.setFlightSector(Integer.parseInt(tagContent));
                                break;
                            case "color":
                                currentCity.setColor(tagContent);
                                break;
                            case "cityNode":
                                cities.put(currentCity.getName(),currentCity);
                                break;
                            default:
                                //do nothing
                                break;
                        }
                        break;
                    case XMLStreamConstants.START_DOCUMENT:
//                        cityList = new ArrayList<>();
                    default:
                        //do nothing
                        break;

                }
            }

        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (FileNotFoundException f){
            System.out.println("oh shit");
        }
        res.setCities(cities);
    }
    
    public static void loadCityCoordinates(){
        JTGameResources res = JTGameResources.getInstance();
//        String[][] cityLocations = new String[180][3];
        try {
            Scanner csvFile = new Scanner(new File("game_resources/cityLocations.csv"));
            if(csvFile.hasNextLine()) {
                for (int i = 0; i < 179; i++) {
                    String[] oneCityLocation = csvFile.nextLine().trim().split(",");

                    JTCity city = res.getCities().get(oneCityLocation[0].toString());
                    city.setxCoordinate(Double.parseDouble(oneCityLocation[1])/2.0);
                    city.setyCoordinate(Double.parseDouble(oneCityLocation[2])/2.0);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static void saveGame(){
        JTGameResources res = JTGameResources.getInstance();
        JTGameStateManager gsm = JTGameStateManager.getInstance();
        JTUserInterface ui = JTUserInterface.getInstance();

        File saveFile = new File(SAVE_FILE);
        try {
            if(saveFile.exists()){
                saveFile.delete();
                saveFile.createNewFile();
            }
            PrintWriter writer = new PrintWriter(SAVE_FILE);
            writer.println(res.getPlayers().length);
            writer.println(gsm.getTurnCounter());

            for (JTPlayer player : res.getPlayers()){
                if (player instanceof JTHooman){
                    writer.println("Hooman");
                } else {
                    writer.println("Compooter");
                }
                writer.println(player.getColor());
                writer.println(player.getHomeCityName());
                writer.println(player.getPlayerName());
                writer.println(player.getNumCards());
                writer.println(player.getPlayerNumber());
                for (String cardName : player.getCardsInHand()){
                    writer.println(cardName);
                }
                writer.println("BREAKPOINT");
                writer.println(player.getCurrentCityName());
                writer.println(player.hasWonGame());
                writer.println(player.getCurrentXCoord());
                writer.println(player.getCurrentYCoord());
                writer.println(player.getRemainingMoves());
            }

            writer.println(ui.getGameHistoryText().getText());

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void loadGame(){
        JTGameResources res = JTGameResources.getInstance();
        JTGameStateManager gsm = JTGameStateManager.getInstance();
        JTUserInterface ui = JTUserInterface.getInstance();
        File saveFile = new File(SAVE_FILE);
        Scanner scanner = null;
        try {
            scanner = new Scanner(saveFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (saveFile.exists()){
            JTPlayer[] players = new JTPlayer[Integer.parseInt(scanner.nextLine().trim().trim())];
            gsm.setTurnCounter(Integer.parseInt(scanner.nextLine().trim()));
            for (int i = 0; i < players.length; i++) {
                String playerType = scanner.nextLine().trim();
                if (playerType.matches("Hooman"))
                    players[i] = new JTHooman();
                else
                    players[i] = new JTCompooter();
                players[i].setColor(scanner.nextLine().trim());
                players[i].setHomeCityName(scanner.nextLine().trim());
                players[i].setPlayerName(scanner.nextLine().trim());
                players[i].setNumCards(Integer.parseInt(scanner.nextLine().trim()));
                players[i].setPlayerNumber(Integer.parseInt(scanner.nextLine().trim()));
                ArrayList<String> cardsInHand = new ArrayList<>();
                String temp = "";
                while (!temp.matches("BREAKPOINT")){
                    temp = scanner.nextLine().trim();
                    if(!temp.matches("BREAKPOINT"))
                        cardsInHand.add(temp);
                }
                players[i].setCardsInHand(cardsInHand);
                players[i].setCurrentCityName(scanner.nextLine().trim());
                players[i].setHasWonGame(Boolean.parseBoolean(scanner.nextLine().trim()));
                players[i].setCurrentXCoord(Double.parseDouble(scanner.nextLine().trim()));
                players[i].setCurrentYCoord(Double.parseDouble(scanner.nextLine().trim()));
                players[i].setRemainingMoves(Integer.parseInt(scanner.nextLine().trim()));
            }

            StringBuffer buffer = new StringBuffer();

            while (scanner.hasNextLine()){
                buffer.append(scanner.nextLine()).append("\n");
            }

            ui.getGameHistoryText().setText(buffer.toString());

            res.setPlayers(players);

            ui.getGameCanvas().clean();

            gsm.switchScreen(JTGameStateManager.JTScreenState.GAME_SCREEN);

        }
    }

}
