package jtgame.ui;

import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import jtgame.game.JTCity;
import jtgame.game.JTGameResources;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import jtgame.game.JTGameStateManager;
import jtgame.game.JTPlayer;
import jtgame.io.JTFileIO;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Freya on 10/31/14.
 */
public class JTEventHandler {

    private static JTUserInterface ui = JTUserInterface.getInstance();



    public static void respondToNumPlayersSelected(String s){
        /*Get the JTUserInterface*/
        ui = JTUserInterface.getInstance();

        /*Clear out any panes and their contents that might already be in there*/
        ui.getPlayerSelectionPane().getChildren().clear();
        ui.getPlayerFlagImages().clear();
        ui.getPlayerSelectRadioButtonGroups().clear();
        ui.getPlayerSelectRadioButtons().clear();
        ui.getPlayerSelectNameTextFields().clear();

        /*Figure out how many players there are*/
        int numberOfPlayers = Integer.parseInt(s.substring(0,1));

        /*Change the size of all the ArrayLists depending on the number of players*/
        for (int i = 0; i < numberOfPlayers ; i++) {
            ui.getPlayerFlagImages().add(null);
            /*This is a bit of an inelegant solution but we add null twice to the radiobuttons list because there are
            twice as many of them*/
            ui.getPlayerSelectRadioButtons().add(null);
            ui.getPlayerSelectRadioButtons().add(null);
            ui.getPlayerSelectRadioButtonGroups().add(null);
            ui.getPlayerSelectNameTextFields().add(null);

        }

        /*Create an array for that many players and set all their sizes to the correct size.*/
        Pane[] paneArray = new Pane[numberOfPlayers];
        for (int i = 0; i < numberOfPlayers; i++) {
            paneArray[i] = new Pane();
            paneArray[i].setId("player-setup-pane");
            paneArray[i].setMinSize(500,425);
            paneArray[i].setMaxSize(500,425);

        }

        /*
        Now depending on how many players there are, create that many panes.
        NOTE: This is implemented as a cascading switch statement so if there are 6 players, the switch statement will
        cascade all the way down to 1.
         */
        switch (numberOfPlayers){
            case 6:
                createPlayerPaneForGameSetupScreen(paneArray,6,"black");
            case 5:
                createPlayerPaneForGameSetupScreen(paneArray,5,"blue");
            case 4:
                createPlayerPaneForGameSetupScreen(paneArray,4,"green");
            case 3:
                createPlayerPaneForGameSetupScreen(paneArray,3,"red");
            case 2:
                createPlayerPaneForGameSetupScreen(paneArray,2,"white");
            case 1:
                createPlayerPaneForGameSetupScreen(paneArray,1,"yellow");
        }

        /*Add the player selection panes to the grid pane in JTUserInterface*/
        addPaneToGameSetupScreen(paneArray,numberOfPlayers);


    }


    private static void addPaneToGameSetupScreen(Pane[] p, int numberOfPlayers){
        switch(numberOfPlayers){
            case 6:
                ui.getPlayerSelectionPane().getChildren().add(p[5]);
                p[5].setLayoutY(425);
                p[5].setLayoutX(1050);
            case 5:
                ui.getPlayerSelectionPane().getChildren().add(p[4]);
                p[4].setLayoutY(425);
                p[4].setLayoutX(550);
            case 4:
                ui.getPlayerSelectionPane().getChildren().add(p[3]);
                p[3].setLayoutY(425);
                p[3].setLayoutX(50);
            case 3:
                ui.getPlayerSelectionPane().getChildren().add(p[2]);
                p[2].setLayoutY(0);
                p[2].setLayoutX(1050);
            case 2:
                ui.getPlayerSelectionPane().getChildren().add(p[1]);
                p[1].setLayoutY(0);
                p[1].setLayoutX(550);
            case 1:
                ui.getPlayerSelectionPane().getChildren().add(p[0]);
                p[0].setLayoutY(0);
                p[0].setLayoutX(50);

        }
    }

    private static void createPlayerPaneForGameSetupScreen(Pane[] pArray, int playerNumber, String flagColor){
        /*Create the image of the player and send it to the JTUserInterface*/
        //TODO:Get the flag path from a file.
        ImageView playerFlag = new ImageView(new File("artwork/flag_"+flagColor+".png").toURI().toString());
        ui.getPlayerFlagImages().set(playerNumber-1,playerFlag);

                /*Create the radio buttons for selecting between a human player or an AI. Additionally send them to the
                * JTUserInterface*/
        RadioButton humanRadButt = new RadioButton("Human");
        RadioButton computerRadButt = new RadioButton("Computer");
        ToggleGroup group = new ToggleGroup();
        group.getToggles().addAll(humanRadButt,computerRadButt);
        ui.getPlayerSelectRadioButtonGroups().set(playerNumber-1,group);
        ui.getPlayerSelectRadioButtons().set(2*playerNumber-1,humanRadButt);
        ui.getPlayerSelectRadioButtons().set(2*playerNumber-2,computerRadButt);

                /*Create a label to be above the textfield that indicates where the player should enter their name*/
        Label nameLabel = new Label("Name:");

                /*Create the player name text field and send it to the jtgame.ui*/
        TextField playerTextField = new TextField("Player " + playerNumber);
        ui.getPlayerSelectNameTextFields().set(playerNumber-1,playerTextField);

        /*Add everything to the appropriate pane and set them to their proper locations.*/
        pArray[playerNumber-1].getChildren().addAll(playerFlag, humanRadButt, computerRadButt, nameLabel, playerTextField);
        playerFlag.setLayoutX(0);
        playerFlag.setLayoutY(125);
        humanRadButt.setLayoutX(200);
        humanRadButt.setLayoutY(125);
        computerRadButt.setLayoutX(200);
        computerRadButt.setLayoutY(175);
        nameLabel.setLayoutX(350);
        nameLabel.setLayoutY(125);
        playerTextField.setLayoutX(350);
        playerTextField.setLayoutY(175);
    }

    public static void respondToGoButtonPressed() {
        JTGameResources res = JTGameResources.getInstance();
        res.initPlayers();
        JTGameStateManager gsm = JTGameStateManager.getInstance();
        gsm.startTurn();
        ui = JTUserInterface.getInstance();
        ui.runCardAnimation();

    }

    public static void respondToClickOnMap(MouseEvent me, int currentPlayerIndex){
        JTGameResources res = JTGameResources.getInstance();
        JTGameStateManager gsm = JTGameStateManager.getInstance();
        ui = JTUserInterface.getInstance();
        ui.getGameCanvas().clean();

        /*Only color adjacent cities if player has mvoes left*/
        if(res.getPlayers()[gsm.getTurnCounter()].getRemainingMoves() > 0)
            ui.getGameCanvas().colorAdjacentCities();

        Iterator it = res.getCities().entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry<String, JTCity> pair = (HashMap.Entry) it.next();
            if (Math.pow(me.getX() - pair.getValue().getxCoordinate(), 2) + Math.pow(me.getY() - pair.getValue().getyCoordinate(), 2) <= Math.pow(30, 2)) {
                gsm.processMoveRequest(pair.getKey());
            }
        }

    }

    public static void respondToSaveButtonClicked(){
        JTFileIO.saveGame();
    }

    public static void respondToLoadButtonPressed(){
        JTFileIO.loadGame();
        ui = JTUserInterface.getInstance();
        JTGameStateManager gsm = JTGameStateManager.getInstance();
        gsm.startTurn();
    }

    public static void openFlightMap(){
        Stage stage = new Stage();
        stage.setTitle("Flight Map");
        Pane pane = new Pane();
        Canvas canvas = new Canvas(1120/2,1232/2);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        Image flightPlan = null;
        try {
            flightPlan = new Image(new File("artwork/flight_plan.jpg").toURI().toURL().toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        gc.drawImage(flightPlan,0,0,1120/2,1232/2);

        pane.getChildren().add(canvas);

        Scene scene = new Scene(pane,1120/2,1232/2);

        stage.setScene(scene);

        stage.show();
    }
}
