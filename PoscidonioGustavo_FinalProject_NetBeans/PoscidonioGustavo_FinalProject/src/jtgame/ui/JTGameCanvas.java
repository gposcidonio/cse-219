package jtgame.ui;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Timer;
import java.util.TimerTask;
import javafx.animation.*;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Duration;
import jtgame.game.JTCity;
import jtgame.game.JTGameResources;
import jtgame.game.JTGameStateManager;
import jtgame.game.JTPlayer;

/**
 * Created by Gustavo Poscidonio on 11/18/2014.
 */
public class JTGameCanvas extends Canvas {
    private GraphicsContext gc;
    private Image gameMap;


    public JTGameCanvas(double width, double height){
        super(width, height);
        gc = this.getGraphicsContext2D();
        try {
            gameMap = new Image(new File("artwork/gameplay.jpg").toURI().toURL().toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


    }

    public void clean(){
        gc.clearRect(0,0,this.getWidth(),this.getHeight());
        gc.drawImage(gameMap,0,0,3904.0/2,5104.0/2);
        JTGameResources res = JTGameResources.getInstance();
        for (JTPlayer player : res.getPlayers()){
            Image playerFlag = null;
            Image playerPiece = null;

            try {
                playerFlag = new Image(new File("artwork/flag_" + player.getColor() + ".png").toURI().toURL().toString());
                playerPiece = new Image(new File("artwork/piece_" + player.getColor() + ".png").toURI().toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            gc.drawImage(playerFlag, res.getCities().get(player.getHomeCityName()).getxCoordinate() - 37.5, res.getCities().get(player.getHomeCityName()).getyCoordinate() - 75, 75, 75);
            gc.drawImage(playerPiece,player.getCurrentXCoord()-37.5,player.getCurrentYCoord()-75,75,75);
        }
        
    }

    public void movePlayer(String cityName){
        /*Get all the singletons we need. Which is all of them.*/
        JTGameResources res = JTGameResources.getInstance();
        JTGameStateManager gsm = JTGameStateManager.getInstance();
        JTGameCanvas canvas = this;
        gc = this.getGraphicsContext2D();

        /*Here we denote the difference in the X and Y values of our current city and our goal city. Additionally
        * we define two DoubleProperty variables that indicate what our starting distance is from our starting city.
        * This is quite obviously 0 since we are actually ON the city.*/
        double deltaX = res.getCities().get(cityName).getxCoordinate() - res.getPlayers()[gsm.getTurnCounter()].getCurrentXCoord();
        double deltaY = res.getCities().get(cityName).getyCoordinate() - res.getPlayers()[gsm.getTurnCounter()].getCurrentYCoord();
        DoubleProperty dX = new SimpleDoubleProperty(0);
        DoubleProperty dY = new SimpleDoubleProperty(0);


        /*Here we set up the timeline which changes the value of dX to the value of deltaX (identical functionality for
        * Y values) over the course of 600 milliseconds. In other words, it expands the value of dX until it is exactly
        * the horizontal distance between our starting city and our goal city.*/
        Timeline timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(600),
                new KeyValue(dX,deltaX),
                new KeyValue(dY,deltaY)));
        timeline.setCycleCount(1);

        /*Throughout our animation, we will need to refer to the coordinates where we started but we also need to change
        * the player's X and Y coordinates to the new city. To achieve this, we first hold the values of the player's X
        * and Y coordinates in two variables then we change the player's coordinates. Throughout the animation the
        * animation will refer to the variables holding the player's intial location.*/
        JTPlayer player = res.getPlayers()[gsm.getTurnCounter()];
        double xCoordHolder = player.getCurrentXCoord();
        double yCoordHolder = player.getCurrentYCoord();
        player.setCurrentXCoord(res.getCities().get(cityName).getxCoordinate());
        player.setCurrentYCoord(res.getCities().get(cityName).getyCoordinate());


        /*Here we set up the AnimationTimer that will carry out the actual animation. On set intervals, this will draw
        * the player's location. But since the Timeline is constantly changing the value, the AnimationTimer will
        * consistently paint the player in a new location.*/
        AnimationTimer timer;
        timer = new AnimationTimer(){
            JTPlayer p = null;
            @Override
            public void handle(long now){

                if(p != res.getPlayers()[gsm.getTurnCounter()]){
                    p= res.getPlayers()[gsm.getTurnCounter()];
                }
                gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                gc.drawImage(gameMap,0,0,3904.0/2,5104.0/2);


                Image playerPiece = null;
                try {
                    playerPiece = new Image(new File("artwork/piece_" + p.getColor() + ".png").toURI().toURL().toString());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                gc.drawImage(playerPiece,xCoordHolder-37.5+dX.get(),yCoordHolder-75+dY.get(),75,75);

                Image playerFlag = null;
                Image otherPlayerPiece = null;
                for (JTPlayer otherplayer : res.getPlayers()){
                    if(!p.getPlayerName().matches(otherplayer.getPlayerName())){
                        try {
                            playerFlag = new Image(new File("artwork/flag_" + otherplayer.getColor() + ".png").toURI().toURL().toString());
                            otherPlayerPiece = new Image(new File("artwork/piece_" + otherplayer.getColor() + ".png").toURI().toURL().toString());
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                        gc.drawImage(playerFlag, res.getCities().get(otherplayer.getHomeCityName()).getxCoordinate() - 37.5, res.getCities().get(otherplayer.getHomeCityName()).getyCoordinate() - 75, 75, 75);
                        gc.drawImage(otherPlayerPiece,otherplayer.getCurrentXCoord()-37.5,otherplayer.getCurrentYCoord()-75,75,75);
                    } else {

                        try {
                            playerFlag = new Image(new File("artwork/flag_" + otherplayer.getColor() + ".png").toURI().toURL().toString());
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                        gc.drawImage(playerFlag, res.getCities().get(otherplayer.getHomeCityName()).getxCoordinate() - 37.5, res.getCities().get(otherplayer.getHomeCityName()).getyCoordinate() - 75, 75, 75);
                    }
                }

                if(p.getRemainingMoves() > 0){
                    colorAdjacentCities();
                }

                if(dX.doubleValue() - deltaX < 1.0 && dY.doubleValue() - deltaY < 1.0) {
                    dX.set(deltaX);
                    dY.set(deltaY);
                }
                if(dX.doubleValue() - deltaX == 0 && dY.doubleValue() - deltaY == 0){
                    this.stop();
//                    done = true;
                }
            }
        };

//        if(done){
//            timer = null;
//            done = false;
//        }
        /*Start the timeline and the timer*/
        timer.start();
        timeline.play();


        this.clean();


    }

    public static boolean done = false;

    public void colorAdjacentCities(){
        JTGameResources res = JTGameResources.getInstance();
        JTGameStateManager gsm = JTGameStateManager.getInstance();
        JTPlayer player = res.getPlayers()[gsm.getTurnCounter()];
        for (String adjacentCity : res.getCities().get(player.getCurrentCityName()).getLandAdjacancies()){
            gc.setStroke(Color.WHITE);
            gc.setLineWidth(5);
            gc.strokeOval(res.getCities().get(adjacentCity).getxCoordinate()-12,res.getCities().get(adjacentCity).getyCoordinate()-12,25,25);
        }
        for (String adjacentCity : res.getCities().get(player.getCurrentCityName()).getHarborAdjacancies()){
            gc.setStroke(Color.WHITE);
            gc.setLineWidth(5);
            gc.strokeOval(res.getCities().get(adjacentCity).getxCoordinate()-12,res.getCities().get(adjacentCity).getyCoordinate()-12,25,25);
        }
        for (String adjacentCity : res.getCities().get(player.getCurrentCityName()).getAirAdjacencies().keySet()){
            gc.setFill(Color.WHITE);
            gc.setFont(Font.font("monospace", FontWeight.BOLD,40));
            gc.fillText(""+res.getCities().get(player.getCurrentCityName()).getAirAdjacencies().get(adjacentCity),res.getCities().get(adjacentCity).getxCoordinate()-12,res.getCities().get(adjacentCity).getyCoordinate()-12);
        }
    }
}
