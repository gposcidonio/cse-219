package jtgame.ui;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.Scene;
import javafx.scene.canvas.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.*;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;
import jtgame.game.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class creates and controls the UI for the entire JTE Application.
 */
public class JTUserInterface extends Pane{
    //This class will hold an instance of itself to maintain itself as a singleton for ease of programming.
    private static JTUserInterface singleton;

    //The top level pane inside which all other panes will be held
    private Stage primaryStage;
    private StackPane mainPane;

    //Video Background Elements
    private Pane videoPane;
    private MediaView videoBackground;
    private File splashScreenVideoFile;
    private Media splashScreenVideo;
    private MediaPlayer splashScreenMediaPlayer;


    //Splash Screen Elements
    private Pane splashScreenPane;
    private VBox splashScreenSelectionBox;
    private Button startButton;
    private Button loadButton;
    private Button aboutSplashButton;
    private Button quitButton;



    //Game Setup Screen Elements
    private BorderPane gameSetupPane;
    private Pane playerSelectionPane;
    private FlowPane northSetupBar;
    private Label numPlayerLabel;
    private ComboBox numPlayersComboBox;
    private Button goButton;
    private ArrayList<ImageView> playerFlagImages;
    private ArrayList<ToggleGroup> playerSelectRadioButtonGroups;
    private ArrayList<RadioButton> playerSelectRadioButtons;
    private Label playerSelectNameLabel;
    private ArrayList<TextField> playerSelectNameTextFields;

    // In-Game Elements
    private BorderPane gamePlayPane;
    private ScrollPane gamePlayScrollPane;
    private Button gameHistoryButton;
    private Button cityDescriptionsButton;
    private Button aboutGameButton;
    private JTGameCanvas gameCanvas;
    private GraphicsContext gc;
    private Label playerNameLabel;
    private Button saveButton;
    private JTEventHandler eventHandler;
    private Image gameMap;
    private Pane mapPane;
    private ArrayList<Button> cityNodes;
    private Pane leftPane;
    private Pane rightPane;
    private Button dieRoll;
    private Label numMovesLabel;
    private Button checkFlightMapButton;

    //Game History Elements
    private ScrollPane gameHistoryScrollPane;
    private Pane gameHistoryPane;
    private Label gameHistoryText;

    //City Description Items
    private ScrollPane cityDescriptionsScrollPane;
    private Pane cityDescriptionsPane;
    private Label cityDescriptionsText;

    //Left Pane Card imageviews
    private ArrayList<ImageView> leftImageViews = new ArrayList<>();



    //About Pane Elements
//    private Pane aboutPane; <-- I'm going to implement it so that when someone clicks the about button, it just opens the wiki page in their browser.

    /**
     * The default, and only, constructor for the JTUserInterface class. This will call all the initialization methods
     * necessary for the program run and render correctly.
     */
    private JTUserInterface(){
        mainPane = new StackPane();
        eventHandler = new JTEventHandler();
        initVideoPane();
        initSplashScreen();
        System.out.println("here");
        initGameSetupScreen();
        System.out.println("there");
        initGameScreen();
        initGameHistoryPane();
        initCityDescriptionsPane();
        changeUIPane(JTGameStateManager.JTScreenState.SPLASH_SCREEN);
    }

    public static JTUserInterface getInstance(){
        if (singleton == null)
            singleton = new JTUserInterface();
        return singleton;
    }

    public void initVideoPane(){
        videoPane = new Pane();
        /*
        Create and play the video file for the background
        TODO: Make this reference something in a file so you could theoretically replace the video with another 16:9 video
         */
        splashScreenVideoFile = new File("media/CloudsAndGrassH264.mp4");
        splashScreenVideo = new Media(splashScreenVideoFile.toURI().toString());
        splashScreenMediaPlayer = new MediaPlayer(splashScreenVideo);
        videoBackground = new MediaView(splashScreenMediaPlayer);
        splashScreenMediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
        splashScreenMediaPlayer.setAutoPlay(true);

        videoPane.getChildren().add(videoBackground);
        mainPane.getChildren().add(videoPane);
    }

    /**
     * Initializes the Splash Screen and all of its elements.
     */
    public void initSplashScreen(){
        /*Create the pane*/
        splashScreenPane = new Pane();



        /*
        Create the box that contains the options for starting a newgame and assin it a correct CSS class. Additionally
        make all the items inside the box align to the center of the box.
         */
        splashScreenSelectionBox = new VBox();
        splashScreenSelectionBox.getStyleClass().add("vbox");
        splashScreenSelectionBox.setAlignment(Pos.CENTER);

        /*Create all the buttons for the splash screen.*/
        startButton = new Button("Start");
        loadButton = new Button("Load");
        aboutSplashButton = new Button("About");
        quitButton = new Button("Quit");


        /*Assign actions to all the different buttons.*/
        quitButton.setOnAction((ActionEvent e) -> System.exit(0));

        startButton.setOnAction((ActionEvent e) -> {
            changeUIPane(JTGameStateManager.JTScreenState.SETUP_SCREEN);
        });

        loadButton.setOnAction((ActionEvent e) -> {
            JTEventHandler.respondToLoadButtonPressed();
        });

        aboutSplashButton.setOnAction((ActionEvent e) -> {
            if(Desktop.isDesktopSupported()){
                try {
                    Desktop.getDesktop().browse(new URI("http://en.wikipedia.org/wiki/Journey_Through_Europe"));
                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (URISyntaxException e1) {
                    e1.printStackTrace();
                }
            }
        });

        /*Set the CSS ID for all the buttons*/
        startButton.setId("rich-blue");
        loadButton.setId("rich-blue");
        aboutSplashButton.setId("rich-blue");
        quitButton.setId("rich-blue");

        /*Add everything to its appropriate parent in the correct order so that things render correctly.*/
        splashScreenSelectionBox.getChildren().addAll(startButton,loadButton,aboutSplashButton,quitButton);
        splashScreenPane.getChildren().add(splashScreenSelectionBox);
        mainPane.getChildren().add(splashScreenPane);
    }

    /**
     * Initializes the Gameplay Screen and all of tis elements.
     */
    public void initGameScreen(){
        /*TODO:Annotate this block of code with comments.*/
        JTGameResources res = JTGameResources.getInstance();

        cityNodes = new ArrayList();
        gamePlayPane = new BorderPane();
        gamePlayScrollPane = new ScrollPane();
        gameCanvas = new JTGameCanvas(3904.0/2,5104.0/2);
        gc = gameCanvas.getGraphicsContext2D();
        try {
            gameMap = new Image(new File("artwork/gameplay.jpg").toURI().toURL().toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        gc.drawImage(gameMap,0,0,3904.0/2,5104.0/2);

        gamePlayScrollPane.setPannable(true);
        gamePlayScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        gamePlayScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        mapPane = new Pane();
        mapPane.getChildren().add(gameCanvas);
        gamePlayScrollPane.setContent(mapPane);
        gamePlayPane.setCenter(gamePlayScrollPane);

        leftPane = new Pane();
        leftPane.setMinWidth(250);
        leftPane.setMaxWidth(250);

        rightPane = new Pane();
        rightPane.setMinWidth(250);
        rightPane.setMaxWidth(250);


        numMovesLabel = new Label("Moves Remaining: 0");
        numMovesLabel.setId("large-blue-text");
        numMovesLabel.setLayoutX(0);
        numMovesLabel.setLayoutY(200);

        aboutSplashButton.setLayoutX(0);
        aboutSplashButton.setLayoutY(100);

        dieRoll = new Button();
        try {
            dieRoll.setGraphic(new ImageView(new Image(new File("artwork/die_" + res.getDie().getSideFacingUp() + ".jpg").toURI().toURL().toString())));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        dieRoll.setOnAction((ActionEvent e)->{
            JTGameStateManager gsm = JTGameStateManager.getInstance();
            gameCanvas.clean();
            int roll = res.getDie().roll();
            addHistoryText(gsm.getCurrentPlayer().getPlayerName() + " rolled a " + roll + ".");
            numMovesLabel.setText("Moves Remaining: " + roll);
            try {
                dieRoll.setGraphic(new ImageView(new Image(new File("artwork/die_" + res.getDie().getSideFacingUp() + ".jpg").toURI().toURL().toString())));
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            }

            dieRoll.setDisable(true);
            res.getPlayers()[gsm.getTurnCounter()].setRemainingMoves(roll);
            gsm.enableMoving();
            gameCanvas.colorAdjacentCities();
        });
        dieRoll.setLayoutX(0);
        dieRoll.setLayoutY(300);

        saveButton = new Button("Save");
        saveButton.setId("rich-blue");
        saveButton.setLayoutY(450);

        saveButton.setOnAction((ActionEvent e) -> {
            JTEventHandler.respondToSaveButtonClicked();
        });

        checkFlightMapButton = new Button("Check Flight Map");
        checkFlightMapButton.setId("rich-blue");
        checkFlightMapButton.setLayoutY(550);

        checkFlightMapButton.setOnAction((ActionEvent e) -> {
            JTEventHandler.openFlightMap();
        });

        cityDescriptionsButton = new Button("City Flavor Text");
        cityDescriptionsButton.setId("rich-blue");
        cityDescriptionsButton.setLayoutY(650);

        cityDescriptionsButton.setOnAction((ActionEvent e)->{
            JTGameStateManager gsm = JTGameStateManager.getInstance();
            gsm.switchScreen(JTGameStateManager.JTScreenState.CITY_DESCRIPTIONS);
        });

        gameHistoryButton = new Button("Game History");
        gameHistoryButton.setId("rich-blue");
        gameHistoryButton.setLayoutX(0);
        gameHistoryButton.setLayoutY(0);
        gameHistoryButton.setOnAction((ActionEvent e) -> {
            JTGameStateManager gsm = JTGameStateManager.getInstance();
            gsm.switchScreen(JTGameStateManager.JTScreenState.HISTORY_SCREEN);
        });

//        Button testButton = new Button("Testing: End Game");
//        testButton.setId("rich-blue");
//        testButton.setLayoutY(750);
//        testButton.setOnAction((ActionEvent e) -> {
//            endGame();
//        });

        rightPane.getChildren().addAll(gameHistoryButton, aboutSplashButton,numMovesLabel,dieRoll,saveButton, checkFlightMapButton, cityDescriptionsButton);
        gamePlayPane.setRight(rightPane);
        gamePlayPane.setLeft(leftPane);
        mainPane.getChildren().add(gamePlayPane);
    }

    /**
     * Initializes the Game Setup Screen and all of its elements.
     */
    public void initGameSetupScreen(){
        System.out.println("From jtgame.ui: "+singleton);
        /*Initialize jtgame.game*/
        gameSetupPane = new BorderPane();
        playerSelectionPane = new Pane();
        northSetupBar = new FlowPane();
        numPlayerLabel = new Label("Input number of players: ");
        numPlayersComboBox = new ComboBox();
        goButton = new Button("Go!");
        playerFlagImages = new ArrayList<>();
        playerSelectRadioButtons = new ArrayList<>();
        playerSelectRadioButtonGroups = new ArrayList<>();
        playerSelectNameTextFields = new ArrayList<>();

        numPlayerLabel.setId("default-label");

        /*Add the different possible player scenarios to the combo box.*/
        numPlayersComboBox.getItems().addAll("2 players","3 players","4 players","5 players","6 players");

        /*Send the number of players to the JTEventHandler to be handled.*/
        numPlayersComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                JTEventHandler.respondToNumPlayersSelected(
                        numPlayersComboBox.getSelectionModel().getSelectedItem().toString()
                );
            }
        });

        /*Add an action listener to the Go! button*/
        goButton.setOnAction((ActionEvent e) -> {
            JTEventHandler.respondToGoButtonPressed();
        });

        /*Setup the north tool bar, which contains a combo box for the number of players and a Go! button for commencing
        the game.*/
        northSetupBar.getChildren().addAll(numPlayerLabel, numPlayersComboBox, goButton);
        northSetupBar.setAlignment(Pos.CENTER);

        /*Add everything to the gameSetupPane and */
        gameSetupPane.setCenter(playerSelectionPane);
        gameSetupPane.setTop(northSetupBar);
        gameSetupPane.setId("jtgame.game-setup-screen");
        mainPane.getChildren().add(gameSetupPane);
    }

    public void initGameHistoryPane(){
        gameHistoryPane = new Pane();
        Button backButton = new Button("Return to game.");
        backButton.setId("rich-blue");
        backButton.setOnAction((ActionEvent e) ->{
            changeUIPane(JTGameStateManager.JTScreenState.GAME_SCREEN);
        });


        gameHistoryText = new Label("");
        gameHistoryText.setId("large-blue-text");
        gameHistoryText.setLayoutY(100);

        gameHistoryPane.getChildren().addAll(backButton, gameHistoryText);

        gameHistoryScrollPane = new ScrollPane();
        gameHistoryScrollPane.setContent(gameHistoryPane);



        mainPane.getChildren().addAll(gameHistoryScrollPane);
    }

    public void initCityDescriptionsPane(){
        cityDescriptionsPane = new Pane();
        Button backButton = new Button("Return to game");
        backButton.setId("rich-blue");
        backButton.setOnAction((ActionEvent e) ->{
            changeUIPane(JTGameStateManager.JTScreenState.GAME_SCREEN);
        });

        Scanner sc = null;
        try {
            sc = new Scanner(new File("game_resources/cityFlavorText.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        StringBuffer buffer = new StringBuffer();
        while(sc.hasNextLine()){
            buffer.append(sc.nextLine());
            buffer.append("\n");
        }
        cityDescriptionsText = new Label(buffer.toString());
        cityDescriptionsText.setId("large-blue-text");
        cityDescriptionsText.setLayoutY(100);

        cityDescriptionsPane.getChildren().addAll(backButton,cityDescriptionsText);

        cityDescriptionsScrollPane = new ScrollPane();
        cityDescriptionsScrollPane.setContent(cityDescriptionsPane);

        mainPane.getChildren().add(cityDescriptionsScrollPane);

    }

    public void centerOnCurrentPlayer(int currentPlayerIndex){
//        JTGameResources res = JTGameResources.getInstance();
//        gamePlayScrollPane.setHvalue((res.getPlayers()[currentPlayerIndex].getCurrentXCoord()-800)/gamePlayScrollPane.getWidth());
//        gamePlayScrollPane.setVvalue((res.getPlayers()[currentPlayerIndex].getCurrentYCoord()-450)/gamePlayScrollPane.getHeight());
    }

    public void addHistoryText(String s){
        gameHistoryText.setText(gameHistoryText.getText() + s + "\n");
    }

    public void moveCurrentPlayer(String toThisCity){
        gameCanvas.movePlayer(toThisCity);
    }




    /**
     * Displays the map used to select a city to fly to.
     */
    public void displayFlightMap(){

    }

    /**
     * This method is only ever called by the switchScreen method in JTGameStateManager. It switches to a screen
     * depending on the parameter.
     * @param uistate This holds an enum value that determines which screen to switch to.
     */
    public void changeUIPane(JTGameStateManager.JTScreenState uistate){
        /*
        The way this code works is it sets the things you want to see to visible and the things you don't want to see to
        not visible. In the case of the SPLASH_SCREEN and the SETUP_SCREEN you also want to see a video playing in the
        background so we set that to visible then we call the toFront() method on the video pane as well as the
        pane we're working with so that we see the video but interact with the pane that is on top.
         */
        switch(uistate){
            case SPLASH_SCREEN:
                cityDescriptionsScrollPane.setVisible(false);
                gamePlayPane.setVisible(false);
                gameSetupPane.setVisible(false);
                gameHistoryScrollPane.setVisible(false);
                videoPane.setVisible(true);
                videoPane.toFront();
                splashScreenPane.setVisible(true);
                splashScreenPane.toFront();
                break;
            case GAME_SCREEN:
                cityDescriptionsScrollPane.setVisible(false);
                gameSetupPane.setVisible(false);
                gameHistoryScrollPane.setVisible(false);
                videoPane.setVisible(false);
                splashScreenPane.setVisible(false);
                gamePlayPane.setVisible(true);
                break;
            case ABOUT_SCREEN:
                break;
            case HISTORY_SCREEN:
                cityDescriptionsScrollPane.setVisible(false);
                gamePlayPane.setVisible(false);
                gameSetupPane.setVisible(false);
                splashScreenPane.setVisible(false);
                videoPane.setVisible(false);
                gameHistoryScrollPane.setVisible(true);
                break;
            case SETUP_SCREEN:
                cityDescriptionsScrollPane.setVisible(false);
                gamePlayPane.setVisible(false);
                gameHistoryScrollPane.setVisible(false);
                splashScreenPane.setVisible(false);
                videoPane.setVisible(true);
                videoPane.toFront();
                gameSetupPane.setVisible(true);
                gameSetupPane.toFront();
                break;
            case CITY_DESCRIPTIONS:
                gamePlayPane.setVisible(false);
                gameSetupPane.setVisible(false);
                splashScreenPane.setVisible(false);
                videoPane.setVisible(false);
                gameHistoryScrollPane.setVisible(false);
                cityDescriptionsScrollPane.setVisible(true);
                break;
        }
    }

    public void setLeftPaneForCurrentPlayer(int playerIndex){
        leftImageViews.clear();
        JTGameResources res = JTGameResources.getInstance();
        JTPlayer currentPlayer = res.getPlayers()[playerIndex];

        Button endTurnButton = new Button("End Turn");
        endTurnButton.setId("rich-blue");
        endTurnButton.setOnAction((ActionEvent ae) -> {
            JTGameStateManager gsm = JTGameStateManager.getInstance();
            gsm.endTurn();
        });

        Label playerNameLabel = new Label(currentPlayer.getPlayerName());
        playerNameLabel.setId("large-blue-text");

        leftPane.getChildren().addAll(endTurnButton, playerNameLabel);

        endTurnButton.setLayoutX(0);
        endTurnButton.setLayoutY(0);

        playerNameLabel.setLayoutX(0);
        playerNameLabel.setLayoutY(100);

        for (int i = 0; i<currentPlayer.getCardsInHand().size() ; i++){
            String cardName = currentPlayer.getCardsInHand().get(i);
            JTCard card = res.getAllCards().get(cardName);
            Image cardImage = card.getFrontSideImg();
            ImageView cardImageView = new ImageView(cardImage);

            leftImageViews.add(cardImageView);

            cardImageView.setFitHeight(320);
            cardImageView.setFitWidth(225);
            cardImageView.setLayoutX(12.5);
            cardImageView.setLayoutY(900);

            leftPane.getChildren().add(cardImageView);

            /*Don't ask how I figured this math out. I just played with it until it looked right*/
            Timeline timeline = new Timeline();
            timeline.setCycleCount(1);
            timeline.setDelay(Duration.millis(200*i));
            timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200),
                    new KeyValue(cardImageView.translateYProperty(), -700  + (75 * i))));
            timeline.play();
        }
    }

    public void runRemoveAnimation(int index){
        ImageView cardImageView = leftImageViews.get(index);
        Timeline timeline = new Timeline();
        timeline.setCycleCount(1);
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(2000),
                new KeyValue(cardImageView.translateYProperty(), -2000)));
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(600),
                new KeyValue(cardImageView.opacityProperty(), 0)));
        timeline.play();
    }

    /**
     * Plays the initial card distribution animation.
     */
    public void runCardAnimation(){
        JTGameResources res = JTGameResources.getInstance();
        JTPlayer currentPlayer = res.getPlayers()[0];
        Pane tempPane = new Pane();
        mainPane.getChildren().add(tempPane);
        for (int i = 0; i<currentPlayer.getCardsInHand().size() ; i++){
            String cardName = currentPlayer.getCardsInHand().get(i);
            JTCard card = res.getAllCards().get(cardName);
            Image cardImage = card.getFrontSideImg();
            ImageView cardImageView = new ImageView(cardImage);


            tempPane.getChildren().add(cardImageView);
            tempPane.setMinSize(1600,900);
            tempPane.setLayoutX(0);
            tempPane.setLayoutY(0);






            /*Don't ask how I figured this math out. I just played with it until it looked right*/
            cardImageView.setLayoutX(575);
            cardImageView.setLayoutY(130);
            cardImageView.setFitWidth(450);
            cardImageView.setFitHeight(640);
            Timeline timeline = new Timeline();
            timeline.setCycleCount(1);
            timeline.setDelay(Duration.millis(200*i));
            timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200),
                    new KeyValue(cardImageView.translateYProperty(), (75 * (i-1))),
                    new KeyValue(cardImageView.translateXProperty(), -675),
                    new KeyValue(cardImageView.scaleXProperty(),0.5),
                    new KeyValue(cardImageView.scaleYProperty(),0.5)));
            timeline.play();

            tempPane.getChildren().remove(cardImageView);

            leftPane.getChildren().add(cardImageView);

        }
        mainPane.getChildren().remove(tempPane);
    }

    public void endGame(){
        JTGameStateManager gsm = JTGameStateManager.getInstance();
        Pane pane = new Pane();
        pane.setOpacity(0);

        Button endButton = new Button("Thanks for playing!");
        endButton.setId("rich-blue");
        endButton.setLayoutX(350);
        endButton.setOnAction((ActionEvent e) -> {
                System.exit(0);
        });

        Stage stage = new Stage();
        stage.setTitle("Flight Map");
        Pane newWindowPane = new Pane();
        Canvas canvas = new Canvas(900,300);
        GraphicsContext gc = canvas.getGraphicsContext2D();

        gc.setFont(Font.font("monospace",70));
        gc.setFill(Color.WHITE);
        gc.fillRect(0,0,1600/2,900/3);
        gc.setFill(Color.DARKBLUE);
        gc.fillText(gsm.getCurrentPlayer().getPlayerName() + " has won!",75,150);

        newWindowPane.getChildren().addAll(canvas,endButton);

        String css = JTUserInterface.class.getResource("/jtgame/style/JTStyleSheet.css").toExternalForm();

        Scene scene = new Scene(newWindowPane,900,300);

        scene.getStylesheets().clear();

        scene.getStylesheets().add(css);



        stage.setScene(scene);

        stage.show();



        mainPane.getChildren().add(pane);

    }


    public MediaView getSplashBackground() {
        return videoBackground;
    }

    public void setSplashBackground(MediaView videoBackground) {
        this.videoBackground = videoBackground;
    }

    public File getSplashScreenVideoFile() {
        return splashScreenVideoFile;
    }

    public void setSplashScreenVideoFile(File splashScreenVideoFile) {
        this.splashScreenVideoFile = splashScreenVideoFile;
    }

    public Media getSplashScreenVideo() {
        return splashScreenVideo;
    }

    public void setSplashScreenVideo(Media splashScreenVideo) {
        this.splashScreenVideo = splashScreenVideo;
    }

    public MediaPlayer getSplashScreenMediaPlayer() {
        return splashScreenMediaPlayer;
    }

    public void setSplashScreenMediaPlayer(MediaPlayer splashScreenMediaPlayer) {
        this.splashScreenMediaPlayer = splashScreenMediaPlayer;
    }

    public Stage getPrimaryStage() { return primaryStage; }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public StackPane getMainPane() {
        return mainPane;
    }

    public Pane getSplashScreenPane() {
        return splashScreenPane;
    }

    public VBox getSplashScreenSelectionBox() {
        return splashScreenSelectionBox;
    }

    public Button getStartButton() {
        return startButton;
    }

    public Button getLoadButton() {
        return loadButton;
    }

    public Button getAboutSplashButton() {
        return aboutSplashButton;
    }

    public Button getQuitButton() {
        return quitButton;
    }

    public BorderPane getGameSetupPane() {
        return gameSetupPane;
    }

    public Pane getPlayerSelectionPane() {
        return playerSelectionPane;
    }

    public Label getNumPlayerLabel() {
        return numPlayerLabel;
    }

    public ComboBox getNumPlayersComboBox() {
        return numPlayersComboBox;
    }

    public Button getGoButton() {
        return goButton;
    }

    public ArrayList<ImageView> getPlayerFlagImages() {
        return playerFlagImages;
    }

    public ArrayList<ToggleGroup> getPlayerSelectRadioButtonGroups() {
        return playerSelectRadioButtonGroups;
    }

    public ArrayList<RadioButton> getPlayerSelectRadioButtons() {
        return playerSelectRadioButtons;
    }

    public Label getPlayerSelectNameLabel() {
        return playerSelectNameLabel;
    }

    public ArrayList<TextField> getPlayerSelectNameTextFields() {
        return playerSelectNameTextFields;
    }

    public ScrollPane getGamePlayPane() {
        return gamePlayScrollPane;
    }

    public Button getGameHistoryButton() {
        return gameHistoryButton;
    }

    public Button getAboutGameButton() {
        return aboutGameButton;
    }

    public JTGameCanvas getGameCanvas() {
        return gameCanvas;
    }

    public Label getPlayerNameLabel() {
        return playerNameLabel;
    }

    public Button getSaveButton() {
        return saveButton;
    }

    public JTEventHandler getEventHandler() {
        return eventHandler;
    }

    public Pane getGameHistoryPane() {
        return gameHistoryPane;
    }

    public Pane getVideoPane() {
        return videoPane;
    }

    public MediaView getVideoBackground() {
        return videoBackground;
    }

    public FlowPane getNorthSetupBar() {
        return northSetupBar;
    }

    public Image getGameMap() {
        return gameMap;
    }


    public Pane getMapPane() {
        return mapPane;
    }

    public static JTUserInterface getSingleton() {
        return singleton;
    }

    public ArrayList<Button> getCityNodes() {
        return cityNodes;
    }

    public Pane getLeftPane() {
        return leftPane;
    }

    public Pane getRightPane() {
        return rightPane;
    }

    public Button getDieRoll() {
        return dieRoll;
    }

    public Label getNumMovesLabel() {
        return numMovesLabel;
    }

    public Label getGameHistoryText() {
        return gameHistoryText;
    }

    public Button getCheckFlightMapButton() {
        return checkFlightMapButton;
    }

}
