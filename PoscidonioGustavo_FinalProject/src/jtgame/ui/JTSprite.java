package jtgame.ui;


import javafx.scene.image.ImageView;

import java.io.File;

/**
 * Created by Gustavo Poscidonio on 11/10/2014.
 */
public class JTSprite extends ImageView {

    public JTSprite(String filePath){
        super(new File(filePath).toURI().toString());
    }

}
