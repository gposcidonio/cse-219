package jtgame.game;

import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import jtgame.ui.JTUserInterface;

import java.io.File;
import java.net.MalformedURLException;
import java.util.*;

/**
 * Created by Freya on 11/5/14.
 */
public class JTCompooter extends JTPlayer{
    public JTCompooter(String color, String homeCityName, int numCards, ArrayList<String> cardsInHand, String currentCityName, boolean hasWonGame, double currentXCoord, double currentYCoord, int playerNumber) {
        super(color, homeCityName, numCards, cardsInHand, currentCityName, hasWonGame, currentXCoord, currentYCoord, playerNumber);
    }

    private ArrayList<Vertex> listOfVertices = new ArrayList<>();
    private Vertex currentCity;

    public JTCompooter(){
        buildVertexGraph();
    }

    private Vertex getVertex(String name){
        for (Vertex v : listOfVertices){
            if(v.name.matches(name)){
                return v;
            }
        }
        System.out.println("Returning null from getVertex(" + name + ")");
        return null;
    }

    @Override
    public void executeTurn(){
        reset();
        JTGameResources res = JTGameResources.getInstance();
        JTGameStateManager gsm = JTGameStateManager.getInstance();
        currentCity = null;
        Vertex targetCity = null;
        for (Vertex v : listOfVertices){
            if (v.name.matches(this.currentCityName)){
                currentCity = v;
            }
            if (v.name.matches(this.getCardsInHand().get(this.getCardsInHand().size()-1))){
                targetCity = v;
            }
        }
        Dijkstra.computePaths(currentCity);

        ArrayList<Vertex> path = Dijkstra.getShortestPathTo(targetCity);


        rollDice();
        path.remove(0);

//        while(remainingMoves > 0 && remainingMoves > currentCity.getEdgeTo(path.get(0).name).weight){
//
//            gsm.processMoveRequest(path.get(0).name);
//            currentCity = getVertex(path.get(0).name);
//            path.remove(0);
//            gsm.endTurn();
//        }
        josh(path);
//        gsm.endTurn();
//        processMove(remainingMoves,currentCity,path);
//        gsm.endTurn();
    }

    public void josh(ArrayList<Vertex> path){
        JTGameStateManager gsm = JTGameStateManager.getInstance();
        JTUserInterface ui = JTUserInterface.getInstance();
//        int i = 1;
        if(!path.isEmpty() && remainingMoves > 0 && remainingMoves >= currentCity.getEdgeTo(path.get(0).name).weight){


//            final Vertex q = currentCity;

            TranslateTransition tt = new TranslateTransition(Duration.millis(1000), new Button());
//            tt.setDelay(Duration.millis(1000));
            tt.setOnFinished(new EventHandler<ActionEvent>(){

                @Override
                public void handle(ActionEvent event) {
                    System.out.println("currentCity.name = " + currentCity.name);
                    System.out.println("path.get(0) = " + path.get(0));
                    gsm.processMoveRequest(path.get(0).name);
//                    currentCity = getVertex(path.get(0).name);
                    changeCurrentCity(path);
                    path.remove(0);

                    josh(path);
                    ui.getGameCanvas().clean();

                }
            });

            tt.play();
//            i++;
        } else {
            TranslateTransition tt = new TranslateTransition(Duration.millis(1000), new Button());
            tt.setOnFinished(new EventHandler<ActionEvent>(){

                @Override
                public void handle(ActionEvent event) {
                    gsm.endTurn();

                }
            });

            tt.play();
        }
    }

    public void changeCurrentCity(ArrayList<Vertex> path){
        currentCity = getVertex(path.get(0).name);
    }

//    private void processMove(int moves, Vertex currentCity, ArrayList<Vertex> path){
//        JTGameStateManager gsm = JTGameStateManager.getInstance();
//        double cost = currentCity.getEdgeTo(path.get(0).name).weight;
//        if (moves > 0 && moves > cost){
//            gsm.processMoveRequest(path.get(0).name);
//            currentCity = getVertex(path.get(0).name);
//            path.remove(0);
//            processMove(moves - ((int) cost),currentCity,path);
//        } else {
//            return;
//        }
//    }

    private void reset(){
        for (Vertex v : listOfVertices) {
            v.minDistance = Double.POSITIVE_INFINITY;
            v.previous = null;
        }
    }

    private void rollDice(){
        JTUserInterface ui = JTUserInterface.getInstance();
        JTGameStateManager gsm = JTGameStateManager.getInstance();
        JTGameResources res = JTGameResources.getInstance();
        ui.getGameCanvas().clean();
        int roll = res.getDie().roll();
        ui.addHistoryText(gsm.getCurrentPlayer().getPlayerName() + " rolled a " + roll + ".");
        ui.getNumMovesLabel().setText("Moves Remaining: " + roll);
        try {
            ui.getDieRoll().setGraphic(new ImageView(new Image(new File("artwork/die_" + res.getDie().getSideFacingUp() + ".jpg").toURI().toURL().toString())));
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        }

        ui.getDieRoll().setDisable(true);
        this.setRemainingMoves(roll);
        gsm.enableMoving();
        ui.getGameCanvas().colorAdjacentCities();
    }

    private void buildVertexGraph(){
        JTGameResources res = JTGameResources.getInstance();

        Iterator it = res.getCities().values().iterator();
        while(it.hasNext()){
            JTCity city = (JTCity)it.next();
            Vertex v = new Vertex(city.getName());
            listOfVertices.add(v);
        }

        for (Vertex v : listOfVertices){
            JTCity city = res.getCities().get(v.name);
            for (String cityName : city.getLandAdjacancies()){
                Vertex neighbor = null;
                for (Vertex potentialNeighbor : listOfVertices){
                    if (potentialNeighbor.name.matches(cityName)){
                        neighbor = potentialNeighbor;
                        break;
                    }
                }
                Edge e = new Edge(neighbor,1);
                v.adjacencies.add(e);
            }
            for (String cityName : city.getHarborAdjacancies()){
                Vertex neighbor = null;
                for (Vertex potentialNeighbor : listOfVertices){
                    if (potentialNeighbor.name.matches(cityName)){
                        neighbor = potentialNeighbor;
                        break;
                    }
                }
                Edge e = new Edge(neighbor,6);
                v.adjacencies.add(e);
            }
            for (String cityName : city.getAirAdjacencies().keySet()){
                Vertex neighbor = null;
                for (Vertex potentialNeighbor : listOfVertices){
                    if (potentialNeighbor.name.matches(cityName)){
                        neighbor = potentialNeighbor;
                        break;
                    }
                }
                Edge e = new Edge(neighbor,city.getAirAdjacencies().get(neighbor.name));
                v.adjacencies.add(e);
            }
        }

    }

}
class Vertex implements Comparable<Vertex> {
    public final String name;
    public ArrayList<Edge> adjacencies = new ArrayList<>();
    public double minDistance = Double.POSITIVE_INFINITY;
    public Vertex previous;
    public Vertex(String argName) {
        name = argName;
    }
    public String toString() {
        return name;
    }
    public int compareTo(Vertex other) {
        return Double.compare(minDistance, other.minDistance);
    }

    public Edge getEdgeTo(String vertexName){
        for (Edge e : adjacencies){
            if (e.target.name.matches(vertexName)){
                return e;
            }
        }
        System.out.println("returning null from getEdgeTo(" + vertexName + ")");
        return null;
    }
}

class Edge {
    public final Vertex target;
    public final double weight;
    public Edge(Vertex argTarget, double argWeight) {
        target = argTarget;
        weight = argWeight;
    }
}
class Dijkstra {
    public static void computePaths(Vertex source) {
        source.minDistance = 0.;
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
        vertexQueue.add(source);
        while (!vertexQueue.isEmpty()) {
            Vertex u = vertexQueue.poll();
// Visit each edge exiting u
            for (Edge e : u.adjacencies) {
                Vertex v = e.target;
                double weight = e.weight;
                double distanceThroughU = u.minDistance + weight;
                if (distanceThroughU < v.minDistance) {
                    vertexQueue.remove(v);
                    v.minDistance = distanceThroughU;
                    v.previous = u;
                    vertexQueue.add(v);
                }
            }
        }
    }
    public static ArrayList<Vertex> getShortestPathTo(Vertex target) {
        ArrayList<Vertex> path = new ArrayList<>();
        for (Vertex vertex = target; vertex != null; vertex = vertex.previous)
            path.add(vertex);
        Collections.reverse(path);
        return path;
    }
//    public static void main(String[] args) {
//        Vertex v0 = new Vertex("London");
//        Vertex v1 = new Vertex("Dover");
//        Vertex v2 = new Vertex("Calais");
//        Vertex v3 = new Vertex("Paris");
//        Vertex v4 = new Vertex("Rotterdam");
//        Vertex v5 = new Vertex("Brussels");
//        Vertex v6 = new Vertex("Lille");
//        v0.adjacencies = new Edge[] { new Edge(v1, 1), new Edge(v4, 6), new Edge(v3, 2) };
//        v1.adjacencies = new Edge[] { new Edge(v0, 1), new Edge(v2, 1) };
//        v2.adjacencies = new Edge[] { new Edge(v1, 1), new Edge(v3, 1), new Edge(v6, 1) };
//        v3.adjacencies = new Edge[] { new Edge(v0, 2), new Edge(v2, 1), new Edge(v6, 1) };
//        v4.adjacencies = new Edge[] { new Edge(v0, 6), new Edge(v5, 1) };
//        v5.adjacencies = new Edge[] { new Edge(v4, 1), new Edge(v6, 1) };
//        v6.adjacencies = new Edge[] { new Edge(v3, 1), new Edge(v5, 1) };
//        Vertex[] vertices = { v0, v1, v2, v3, v4, v5, v6 };
//// Paths from London
//        computePaths(v0);
//        for (Vertex v : vertices) {
//            System.out.println("Distance from London to " + v + ": "
//                    + v.minDistance);
//            List<Vertex> path = getShortestPathTo(v);
//            System.out.println("Path: " + path);
//        }
//    }

}
