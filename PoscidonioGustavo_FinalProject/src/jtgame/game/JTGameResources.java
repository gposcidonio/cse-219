package jtgame.game;

import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import jtgame.io.JTFileIO;
import jtgame.ui.JTUserInterface;

import java.io.File;
import java.net.MalformedURLException;
import java.util.*;

/**
 * This class serves as a house for the data needed for the jtgame.game. All the data related to the jtgame.game is in this class so
 * a jtgame.game can be completely loaded by simply loading the information in this class from a file.
 */
public class JTGameResources {

    private static JTGameResources instance = null;
    private static HashMap<String,JTCity> cities;
    private static HashMap<String,JTCard> allCards;
    private static HashMap<String,JTCard> redCards;
    private static HashMap<String,JTCard> greenCards;
    private static HashMap<String,JTCard> yellowCards;
    private static JTPlayer[] players;
    private static JTDie die;
    private static int numPlayers;
    private static double gameWidth;
    private static double gameHeight;

    /**
     * The default constructor for a new JTGameData.
     */
    private JTGameResources(){
        die = new JTDie();
    }


    public synchronized static JTGameResources getInstance(){
        if (instance == null){
            instance = new JTGameResources();
        }

        return instance;
    }

    /**
     * A method that reads information from a file and loads the data accordingly.
     */
    public void loadGameData(){
        //TODO
    }

    public void initCities(){
        JTFileIO.loadCities();
        JTFileIO.loadCityCoordinates();
        initAirAdjacencies();
        Iterator it = cities.entrySet().iterator();
        int count = 1;
        while(it.hasNext()){
            HashMap.Entry<String,JTCity> city = (HashMap.Entry<String,JTCity>)it.next();
            System.out.println(count++ + " " + city.getKey() + " " + city.getValue().getxCoordinate() + " " + city.getValue().getyCoordinate() + " " + city.getValue().getColor());
        }
    }

    public void initCards(){
        Iterator it = cities.entrySet().iterator();
        redCards = new HashMap<>();
        greenCards = new HashMap<>();
        yellowCards = new HashMap<>();
        allCards = new HashMap<>();
        while(it.hasNext()){
            HashMap.Entry<String,JTCity> city = (HashMap.Entry<String,JTCity>)it.next();
            JTCard card = new JTCard();
            switch (city.getValue().getColor()){
                case "green":
                    card.setColor("green");
                    card.setAssociatedCity(city.getValue());
                    try {
                        card.setFrontSideImg(new Image(new File("artwork/green/"+city.getKey()+".jpg").toURI().toURL().toString()));
                        File backSideImage = new File("artwork/green/"+city.getKey()+"_I.jpg");
                        if (backSideImage.exists()){
                            card.setBackSideImg(new Image(backSideImage.toURI().toURL().toString()));
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    greenCards.put(city.getKey(),card);
                    allCards.put(city.getKey(),card);
                    break;
                case "red":
                    card.setColor("red");
                    card.setAssociatedCity(city.getValue());
                    try {
                        card.setFrontSideImg(new Image(new File("artwork/red/"+city.getKey()+".jpg").toURI().toURL().toString()));
                        File backSideImage = new File("artwork/red/"+city.getKey()+"_I.jpg");
                        if (backSideImage.exists()){
                            card.setBackSideImg(new Image(backSideImage.toURI().toURL().toString()));
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    redCards.put(city.getKey(),card);
                    allCards.put(city.getKey(),card);
                    break;
                case "yellow":
                    card.setColor("yellow");
                    card.setAssociatedCity(city.getValue());
                    try {
                        card.setFrontSideImg(new Image(new File("artwork/yellow/"+city.getKey()+".jpg").toURI().toURL().toString()));
                        File backSideImage = new File("artwork/yellow/"+city.getKey()+"_I.jpg");
                        if (backSideImage.exists()){
                            card.setBackSideImg(new Image(backSideImage.toURI().toURL().toString()));
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    yellowCards.put(city.getKey(),card);
                    allCards.put(city.getKey(),card);
                    break;
            }
        }

        Iterator it2 = redCards.entrySet().iterator();
        while (it2.hasNext()){
            System.out.println("red "+((HashMap.Entry<String,JTCard>) it2.next()).getKey());
        }

    }

    public void initPlayers(){
        JTUserInterface ui = JTUserInterface.getInstance();

        if (ui.getNumPlayersComboBox().getSelectionModel().getSelectedItem() != null) {
            boolean youMayProceed = true;
            for (int i = 0; i < Integer.parseInt(ui.getNumPlayersComboBox().getSelectionModel().getSelectedItem().toString().substring(0,1)); i++) {
                if (ui.getPlayerSelectRadioButtonGroups().get(i).getSelectedToggle()==null)
                    youMayProceed = false;
            }
            if(youMayProceed) {
                ui.changeUIPane(JTGameStateManager.JTScreenState.GAME_SCREEN);
                JTGameResources gameResources = JTGameResources.getInstance();
                LinkedList<JTCard> listOfRedCards = new LinkedList<>(redCards.values());
                LinkedList<JTCard> listOfGreenCards = new LinkedList<>(greenCards.values());
                LinkedList<JTCard> listOfYellowCards = new LinkedList<>(yellowCards.values());

                long randomizer = System.nanoTime();
                Random random = new Random(randomizer);

                Collections.shuffle(listOfRedCards,random);
                Collections.shuffle(listOfGreenCards,random);
                Collections.shuffle(listOfYellowCards,random);

                numPlayers = Integer.parseInt(ui.getNumPlayersComboBox().getSelectionModel().getSelectedItem().toString().substring(0, 1));
                players = new JTPlayer[numPlayers];
                for (int i = 0; i < numPlayers; i++) {
                    String playerName = ui.getPlayerSelectNameTextFields().get(i).getText();
                    ToggleGroup hoomanOrCompooterSelector = ui.getPlayerSelectRadioButtonGroups().get(i);
                    boolean isHuman = hoomanOrCompooterSelector.getSelectedToggle().toString().contains("Human") ? true : false;

                    JTPlayer player = isHuman ? new JTHooman() : new JTCompooter();
                    player.setPlayerName(ui.getPlayerSelectNameTextFields().get(i).getText());


                    switch (i){
                        case 0:
                            player.setPlayerNumber(1);
                            player.setColor("yellow");
                            break;
                        case 1:
                            player.setPlayerNumber(2);
                            player.setColor("white");
                            break;
                        case 2:
                            player.setPlayerNumber(3);
                            player.setColor("red");
                            break;
                        case 3:
                            player.setPlayerNumber(4);
                            player.setColor("green");
                            break;
                        case 4:
                            player.setPlayerNumber(5);
                            player.setColor("blue");
                            break;
                        case 5:
                            player.setPlayerNumber(6);
                            player.setColor("black");
                            break;
                    }

                    switch (player.getPlayerNumber()%3){
                        case 0:
                            player.getCardsInHand().add(listOfRedCards.remove().getAssociatedCity().getName());
                            player.getCardsInHand().add(listOfGreenCards.remove().getAssociatedCity().getName());
                            player.getCardsInHand().add(listOfYellowCards.remove().getAssociatedCity().getName());
                            player.setNumCards(3);
                            break;
                        case 1:
                            player.getCardsInHand().add(listOfGreenCards.remove().getAssociatedCity().getName());
                            player.getCardsInHand().add(listOfYellowCards.remove().getAssociatedCity().getName());
                            player.getCardsInHand().add(listOfRedCards.remove().getAssociatedCity().getName());
                            player.setNumCards(3);
                            break;
                        case 2:
                            player.getCardsInHand().add(listOfYellowCards.remove().getAssociatedCity().getName());
                            player.getCardsInHand().add(listOfRedCards.remove().getAssociatedCity().getName());
                            player.getCardsInHand().add(listOfGreenCards.remove().getAssociatedCity().getName());
                            player.setNumCards(3);
                            break;
                    }
                    players[i] = player;

                }

                for (JTPlayer player : players){
                    player.setHomeCityName(player.getCardsInHand().get(0));
                    player.setCurrentCityName(player.getCardsInHand().get(0));
                    player.setCurrentXCoord(gameResources.getCities().get(player.getCurrentCityName().toString()).getxCoordinate());
                    player.setCurrentYCoord(gameResources.getCities().get(player.getCurrentCityName().toString()).getyCoordinate());
                }
            }
            ui.getGameCanvas().clean();
        }
    }

    private void initAirAdjacencies(){
        Iterator it = cities.entrySet().iterator();
        while(it.hasNext()){
            HashMap.Entry<String,JTCity> city = (HashMap.Entry<String,JTCity>)it.next();
            Iterator it2 = cities.entrySet().iterator();
            while(it2.hasNext()){
                HashMap.Entry<String,JTCity> potentiallyAdjacentCity = (HashMap.Entry<String,JTCity>)it2.next();
                /*Commence ugly code because who needs variables*/
                switch(city.getValue().getFlightSector()){
                    case 1:
                        if(potentiallyAdjacentCity.getValue().getFlightSector() == 1){
                            city.getValue().getAirAdjacencies().put(potentiallyAdjacentCity.getKey(),2);
                        } else if (potentiallyAdjacentCity.getValue().getFlightSector() == 2 || potentiallyAdjacentCity.getValue().getFlightSector() == 4){
                            city.getValue().getAirAdjacencies().put(potentiallyAdjacentCity.getKey(),4);
                        }
                        break;
                    case 2:
                        if(potentiallyAdjacentCity.getValue().getFlightSector() == 2){
                            city.getValue().getAirAdjacencies().put(potentiallyAdjacentCity.getKey(),2);
                        } else if (potentiallyAdjacentCity.getValue().getFlightSector() == 1 || potentiallyAdjacentCity.getValue().getFlightSector() == 3){
                            city.getValue().getAirAdjacencies().put(potentiallyAdjacentCity.getKey(),4);
                        }
                        break;
                    case 3:
                        if(potentiallyAdjacentCity.getValue().getFlightSector() == 3){
                            city.getValue().getAirAdjacencies().put(potentiallyAdjacentCity.getKey(),2);
                        } else if (potentiallyAdjacentCity.getValue().getFlightSector() == 2 || potentiallyAdjacentCity.getValue().getFlightSector() == 4|| potentiallyAdjacentCity.getValue().getFlightSector() == 6){
                            city.getValue().getAirAdjacencies().put(potentiallyAdjacentCity.getKey(),4);
                        }
                        break;
                    case 4:
                        if(potentiallyAdjacentCity.getValue().getFlightSector() == 4){
                            city.getValue().getAirAdjacencies().put(potentiallyAdjacentCity.getKey(),2);
                        } else if (potentiallyAdjacentCity.getValue().getFlightSector() == 1 || potentiallyAdjacentCity.getValue().getFlightSector() == 3|| potentiallyAdjacentCity.getValue().getFlightSector() == 5){
                            city.getValue().getAirAdjacencies().put(potentiallyAdjacentCity.getKey(),4);
                        }
                        break;
                    case 5:
                        if(potentiallyAdjacentCity.getValue().getFlightSector() == 5){
                            city.getValue().getAirAdjacencies().put(potentiallyAdjacentCity.getKey(),2);
                        } else if (potentiallyAdjacentCity.getValue().getFlightSector() == 4 || potentiallyAdjacentCity.getValue().getFlightSector() == 6){
                            city.getValue().getAirAdjacencies().put(potentiallyAdjacentCity.getKey(),4);
                        }
                        break;
                    case 6:
                        if(potentiallyAdjacentCity.getValue().getFlightSector() == 6){
                            city.getValue().getAirAdjacencies().put(potentiallyAdjacentCity.getKey(),2);
                        } else if (potentiallyAdjacentCity.getValue().getFlightSector() == 3 || potentiallyAdjacentCity.getValue().getFlightSector() == 5){
                            city.getValue().getAirAdjacencies().put(potentiallyAdjacentCity.getKey(),4);
                        }
                        break;
                    case -1:
                        //do nothing
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public HashMap<String, JTCity> getCities() {
        return cities;
    }

    public static HashMap<String, JTCard> getRedCards() {
        return redCards;
    }

    public static void setRedCards(HashMap<String, JTCard> redCards) {
        JTGameResources.redCards = redCards;
    }

    public static HashMap<String, JTCard> getGreenCards() {
        return greenCards;
    }

    public static void setGreenCards(HashMap<String, JTCard> greenCards) {
        JTGameResources.greenCards = greenCards;
    }

    public static HashMap<String, JTCard> getYellowCards() {
        return yellowCards;
    }

    public static void setYellowCards(HashMap<String, JTCard> yellowCards) {
        JTGameResources.yellowCards = yellowCards;
    }

    public static HashMap<String, JTCard> getAllCards() {
        return allCards;
    }

    public static void setAllCards(HashMap<String, JTCard> allCards) {
        JTGameResources.allCards = allCards;
    }

    public JTPlayer[] getPlayers() {
        return players;
    }

    public JTDie getDie() {
        return die;
    }

    public double getGameWidth() {
        return gameWidth;
    }

    public double getGameHeight() {
        return gameHeight;
    }


    public void setCities(HashMap<String, JTCity> cities) {
        this.cities = cities;
    }


    public void setPlayers(JTPlayer[] players) {
        this.players = players;
    }


    public void setDie(JTDie die) {
        this.die = die;
    }

    public void setGameWidth(double gameWidth) {
        this.gameWidth = gameWidth;
    }

    public void setGameHeight(double gameHeight) {
        this.gameHeight = gameHeight;
    }

}
