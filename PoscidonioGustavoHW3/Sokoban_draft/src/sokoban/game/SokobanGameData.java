package sokoban.game;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.GregorianCalendar;

/**
 * SokobanGameData stores the data necessary for a single Sokoban game. Note
 * that this class works in concert with the SokobanGameStateManager, so all
 * instance variables have default (package-level) access.
 */
public class SokobanGameData {

    private int levelNumber;
    private int gamesPlayed;
    private int wins;
    private int losses;
    private double winRate;
    private long fastestWin;
    
    // START AND END TIME WILL BE USED TO CALCULATE THE
    // TIME IT TAKES TO PLAY THIS GAME
    GregorianCalendar startTime;
    GregorianCalendar endTime;
    GregorianCalendar currentTime;

    // THESE ARE USED FOR FORMATTING THE TIME OF GAME
    final long MILLIS_IN_A_SECOND = 1000;
    final long MILLIS_IN_A_MINUTE = 1000 * 60;
    final long MILLIS_IN_AN_HOUR = 1000 * 60 * 60;

    /*
     * Construct this object when a game begins.
     */
    public SokobanGameData(int levelNumber, int gamesPlayed, int wins, int losses, double winRate, long fastestWin) {
        startTime = new GregorianCalendar();
        currentTime = new GregorianCalendar();
        endTime = null;
        this.levelNumber = levelNumber;
        this.gamesPlayed = gamesPlayed;
        this.wins = wins;
        this.losses = losses;
        this.winRate = winRate;
        if (wins != 0) {
            this.fastestWin = fastestWin;
        } else {
            this.fastestWin = -1;
        }
    }
    
    public SokobanGameData(int levelNumber){
        startTime = new GregorianCalendar();
        currentTime = new GregorianCalendar();
        endTime = null;
        this.levelNumber = levelNumber;
        this.gamesPlayed = 0;
        this.wins = 0;
        this.losses = 0;
        this.winRate = 0;
        this.fastestWin = -1;
        
    }

    // ACCESSOR METHODS
    /**
     * Gets the total time (in milliseconds) that this game took.
     *
     * @return The time of the game in milliseconds.
     */
    public long getTimeOfGame() {
        // IF THE GAME ISN'T OVER YET, THERE IS NO POINT IN CONTINUING
        if (endTime == null) {
            return -1;
        }

        // THE TIME OF THE GAME IS END-START
        long startTimeInMillis = startTime.getTimeInMillis();
        long endTimeInMillis = endTime.getTimeInMillis();

        // CALC THE DIFF AND RETURN IT
        long diff = endTimeInMillis - startTimeInMillis;
        return diff;
    }
    
    public long getCurrentTime(){
        return currentTime.getTimeInMillis();
    }
    
    public void resetTime(){
        startTime = new GregorianCalendar();
        currentTime = new GregorianCalendar();
        endTime = null;
    }

    /**
     * Called when a player quits a game before ending the game.
     */
    public void endTime() {
        endTime = new GregorianCalendar();
    }

    /**
     * Builds and returns a textual summary of this game.
     *
     * @return A textual summary of this game, including the secred word, the
     * time of the game, and a listing of all the guesses.
     */
    @Override
    public String toString() {
        // CALCULATE GAME TIME USING HOURS : MINUTES : SECONDS
        currentTime = new GregorianCalendar();
        long timeInMillis = currentTime.getTimeInMillis() - startTime.getTimeInMillis();
        long hours = timeInMillis / MILLIS_IN_AN_HOUR;
        timeInMillis -= hours * MILLIS_IN_AN_HOUR;
        long minutes = timeInMillis / MILLIS_IN_A_MINUTE;
        timeInMillis -= minutes * MILLIS_IN_A_MINUTE;
        long seconds = timeInMillis / MILLIS_IN_A_SECOND;

        // THEN ADD THE TIME OF GAME SUMMARIZED IN PARENTHESES
        String minutesText = "" + minutes;
        if (minutes < 10) {
            minutesText = "0" + minutesText;
        }
        String secondsText = "" + seconds;
        if (seconds < 10) {
            secondsText = "0" + secondsText;
        }
        String time = hours + ":" + minutesText + ":" + secondsText;
        // TODO add game data
        return time;
    }

    /**
     * @return the levelNumber
     */
    public int getLevelNumber() {
        return levelNumber;
    }

    /**
     * @return the gamesPlayed
     */
    public int getGamesPlayed() {
        return gamesPlayed;
    }

    /**
     * @return the wins
     */
    public int getWins() {
        return wins;
    }

    /**
     * @return the losses
     */
    public int getLosses() {
        return losses;
    }

    /**
     * @return the winRate
     */
    public double getWinRate() {
        return winRate;
    }

    /**
     * @return the fastestWin
     */
    public long getFastestWin() {
        return fastestWin;
    }

    /**
     * @param levelNumber the levelNumber to set
     */
    public void setLevelNumber(int levelNumber) {
        this.levelNumber = levelNumber;
    }

    /**
     * @param gamesPlayed the gamesPlayed to set
     */
    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    /**
     * @param wins the wins to set
     */
    public void setWins(int wins) {
        this.wins = wins;
    }

    /**
     * @param losses the losses to set
     */
    public void setLosses(int losses) {
        this.losses = losses;
    }

    /**
     * @param winRate the winRate to set
     */
    public void setWinRate(double winRate) {
        this.winRate = winRate;
    }

    /**
     * @param fastestWin the fastestWin to set
     */
    public void setFastestWin(long fastestWin) {
        this.fastestWin = fastestWin;
    }
    
    public void saveFile(){
        String filePath = "./data/level" + levelNumber + ".sav";
        File file = new File(filePath);
        try{
            file.createNewFile();
        } catch (IOException ex){
//            ex.printStackTrace();   
            System.out.println("file exists read location");
        }
        try {
            FileOutputStream fos = new FileOutputStream(filePath);
            DataOutputStream dos = new DataOutputStream(fos);
            dos.writeInt(levelNumber);
            dos.writeInt(gamesPlayed);
            dos.writeInt(wins);
            dos.writeInt(losses);
            dos.writeDouble(winRate);
            dos.writeLong(fastestWin);
            dos.flush();
            dos.close();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
