package sokoban.game;

import java.util.ArrayList;
import java.util.Stack;
import javafx.scene.input.KeyCode;
import static javafx.scene.input.KeyCode.DOWN;
import static javafx.scene.input.KeyCode.LEFT;
import static javafx.scene.input.KeyCode.RIGHT;
import static javafx.scene.input.KeyCode.UP;
import sokoban.file.SokobanFileLoader;

import sokoban.ui.SokobanUI;

public class SokobanGameStateManager {

    private static final int WALL = 1;
    private static final int BOX = 2;
    private static final int RED_DOT = 3;
    private static final int SOKO = 4;
    private static final int EMPTY = 5;
    
    public static final int WENTUP = 6;
    public static final int WENTRIGHT = 7;
    public static final int WENTDOWN = 8;
    public static final int WENTLEFT = 9;
    
    //SOKOBAN'S LOCATION YO
    private int sokoX;
    private int sokoY;
    private int underSokosFeet;
    
    //NUMBER OF WIN SPOTS
    private int numRedDots;
    private int numCorrectSpots;
    
    //THIS IS AN INITIAL GRID SNAPSHOP
    private int[][] initGrid;
    
    //THIS CLASS WILL MANAGE THE GRID AND IT'S CONTENTS
    private int[][] grid;
    private int gridRows;
    private int gridColumns;
    public int[][] moveGrid;
    
    //THIS STACK WILL HOLD EVERYTHING FOR UNDO BUTTON
    private Stack<int[][]> undoStack;
    private Stack underSokosFeetStack;
    
    private boolean wonGame = false;

    /**
     * @return the numRedDots
     */
    public int getNumRedDots() {
        return numRedDots;
    }

    /**
     * @param numRedDots the numRedDots to set
     */
    public void setNumRedDots(int numRedDots) {
        this.numRedDots = numRedDots;
    }
    // THE GAME WILL ALWAYS BE IN
    // ONE OF THESE STATES
    public enum SokobanGameState {
        GAME_NOT_STARTED, GAME_IN_PROGRESS, GAME_OVER,
        LEVEL1, 
        LEVEL2, 
        LEVEL3, 
        LEVEL4, 
        LEVEL5, 
        LEVEL6, 
        LEVEL7 
        
    }

    // STORES THE CURRENT STATE OF THIS GAME
    private SokobanGameState currentGameState;

    // WHEN THE STATE OF THE GAME CHANGES IT WILL NEED TO BE
    // REFLECTED IN THE USER INTERFACE, SO THIS CLASS NEEDS
    // A REFERENCE TO THE UI
    private SokobanUI ui;

    // THIS IS THE GAME CURRENTLY BEING PLAYED
    private SokobanGameData gameData;

    // HOLDS ALL OF THE COMPLETED GAMES. NOTE THAT THE GAME
    // IN PROGRESS IS NOT ADDED UNTIL IT IS COMPLETED
    private ArrayList<SokobanGameData> gamesHistory;

    private final String NEWLINE_DELIMITER = "\n";

    public SokobanGameStateManager(SokobanUI initUI) {
        ui = initUI;

        // WE HAVE NOT STARTED A GAME YET
        currentGameState = SokobanGameState.GAME_NOT_STARTED;

        // NO GAMES HAVE BEEN PLAYED YET, BUT INITIALIZE
        // THE DATA STRCUTURE FOR PLACING COMPLETED GAMES
        gamesHistory = new ArrayList();

        // THE FIRST GAME HAS NOT BEEN STARTED YET
        gameData = initUI.gameData;
        
        numCorrectSpots = 0;
        
        undoStack = new Stack();
        
        underSokosFeetStack = new Stack();
    }

    // ACCESSOR METHODS
    /**
     * Accessor method for getting the game currently being played.
     *
     * @return The game currently being played.
     */
    public SokobanGameData getGameData() {
        return gameData;
    }



    /**
     * Accessor method for testing to see if any games have been started yet.
     *
     * @return true if at least one game has already been started during this
     * session, false otherwise.
     */
    public boolean isGameNotStarted() {
        return currentGameState == SokobanGameState.GAME_NOT_STARTED;
    }

    /**
     * Accessor method for testing to see if the current game is over.
     *
     * @return true if the game in progress has completed, false otherwise.
     */
    public boolean isGameOver() {
        return currentGameState == SokobanGameState.GAME_OVER;
    }

    /**
     * Accessor method for testing to see if the current game is in progress.
     *
     * @return true if a game is in progress, false otherwise.
     */
    public boolean isGameInProgress() {
        return currentGameState == SokobanGameState.GAME_IN_PROGRESS;
    }
    
    public void setGameData(SokobanGameData gameData){
        this.gameData = gameData;
    }


    public void endGame() {
        if(isGameInProgress()){
        currentGameState = SokobanGameState.GAME_OVER;
        gameData.endTime();
        if (((gameData.getFastestWin() == -1 || gameData.getTimeOfGame() < gameData.getFastestWin()) && wonGame)) {
            gameData.setFastestWin(gameData.getTimeOfGame());
        } 

        if (wonGame) {
            gameData.setWins(gameData.getWins() + 1);
        } else {
            gameData.setLosses(gameData.getLosses() + 1);
        }
        gameData.setGamesPlayed(gameData.getGamesPlayed() + 1);
        gameData.setWinRate((double)gameData.getWins()/(double)gameData.getGamesPlayed());
        gameData.saveFile();
        }

    }
    

    /**
     * This method chooses a secret word and uses it to create a new game,
     * effectively starting it.
     */
    public void makeNewGame(String filePath) {
        SokobanFileLoader fileLoader = new SokobanFileLoader(filePath,gameData,this);
        // THE GAME IS OFFICIALLY UNDERWAY
        currentGameState = SokobanGameState.GAME_IN_PROGRESS;
    }
    
    public int[][] getGrid(){
        return grid;
    }
    
    public int getGridRows(){
        return gridRows;
    }
    
    public int getGridColumns(){
        return gridColumns;
    }
    
    public int getSokoX(){
        return sokoX;
    }
    
    public int getSokoY(){
        return sokoY;
    }
    
    public int getUnderSokosFeet(){
        return underSokosFeet;
    }
    
    public int[][] getInitGrid(){
        return initGrid;
    }
    
    public void newGrid(int gridRows, int gridColumns, int[][] grid){
        this.moveGrid = new int[gridColumns][gridRows];
        this.initGrid = new int[gridColumns][gridRows];
        for (int i = 0; i < gridColumns; i++) {
            for (int j = 0; j < gridRows; j++) {
                initGrid[i][j] = grid[i][j];
            }
        }
        this.grid = grid;
        this.gridColumns = gridColumns;
        this.gridRows = gridRows;
        underSokosFeet = 5;
        helpMePushGridPrease();
    }
    
    public void updateGrid(int[][] grid){
        this.grid = grid;
    }
    
    public void setGridRows(int gridRows){
        this.gridRows = gridRows;
    }
    
    public void setGridColumns(int gridColumns){
        this.gridColumns = gridColumns;
    }
    
    public void setSokoX(int sokoX){
        this.sokoX = sokoX;
    }
    
    public void setSokoY(int sokoY){
        this.sokoY = sokoY;
    }
    public void resetCorrectSpots(){
        numCorrectSpots = 0;
    }
    
    public void setUnderSokosFeet(int underSokosFeet){
        this.underSokosFeet = underSokosFeet;
    }

    
    public void resolveDatMoveDoe(KeyCode keyCode){
        switch(keyCode){
            case UP:
                System.out.println("UP");
                if (grid[sokoX][sokoY - 1] != WALL) {
                    if (grid[sokoX][sokoY-1] == EMPTY || grid[sokoX][sokoY-1] == RED_DOT) {
                        grid[sokoX][sokoY] = underSokosFeet;
                        underSokosFeet = grid[sokoX][sokoY-1];
                        grid[sokoX][sokoY-1] = SOKO;
                    } else if (grid[sokoX][sokoY-1] == 2){
                        if (grid[sokoX][sokoY-2] == 5 || grid[sokoX][sokoY-2] == RED_DOT) {
                            grid[sokoX][sokoY] = underSokosFeet;
                            if (initGrid[sokoX][sokoY-1] == RED_DOT) {
                                underSokosFeet = RED_DOT;
                            } else {
                                underSokosFeet = EMPTY;
                            }
                            grid[sokoX][sokoY-1] = SOKO;
                            grid[sokoX][sokoY-2] = 2;

                        }else {
                            ui.playBump();
                        }
                    }
                    moveGrid[sokoX][sokoY-1] = WENTUP;
                } else {
                    ui.playBump();
                }
                helpMePushGridPrease();
                break;
            case DOWN:
                System.out.println("DOWN");
                if (grid[sokoX][sokoY + 1] != 1) {
                    if (grid[sokoX][sokoY+1] == 5 || grid[sokoX][sokoY+1] == RED_DOT) {
                        grid[sokoX][sokoY] = underSokosFeet;
                        underSokosFeet = grid[sokoX][sokoY+1];
                        grid[sokoX][sokoY+1] = SOKO;
                    } else if (grid[sokoX][sokoY+1] == 2){
                        if (grid[sokoX][sokoY+2] == 5 || grid[sokoX][sokoY+2] == RED_DOT) {
                            grid[sokoX][sokoY] = underSokosFeet;
                            if (initGrid[sokoX][sokoY+1] == RED_DOT) {
                                underSokosFeet = RED_DOT;
                            } else {
                                underSokosFeet = EMPTY;
                            }
                            grid[sokoX][sokoY+1] = SOKO;
                            grid[sokoX][sokoY+2] = 2;

                        }else {
                            ui.playBump();
                        }
                    }
                    moveGrid[sokoX][sokoY+1] = WENTDOWN;
                } else {
                    ui.playBump();
                }
                helpMePushGridPrease();
                break;
            case LEFT:
                System.out.println("LEFT");
                if (grid[sokoX-1][sokoY] != 1) {
                    if (grid[sokoX-1][sokoY] == 5 || grid[sokoX-1][sokoY] == RED_DOT) {
                        grid[sokoX][sokoY] = underSokosFeet;
                        underSokosFeet = grid[sokoX-1][sokoY];
                        grid[sokoX-1][sokoY] = SOKO;
                    } else if (grid[sokoX-1][sokoY] == 2){
                        if (grid[sokoX-2][sokoY] == 5 || grid[sokoX-2][sokoY] == RED_DOT) {
                            grid[sokoX][sokoY] = underSokosFeet;
                            if (initGrid[sokoX-1][sokoY] == RED_DOT) {
                                underSokosFeet = RED_DOT;
                            } else {
                                underSokosFeet = EMPTY;
                            }
                            grid[sokoX-1][sokoY] = SOKO;
                            grid[sokoX-2][sokoY] = 2;
                            
                        } else {
                            ui.playBump();
                        }
                    }
                    moveGrid[sokoX-1][sokoY] = WENTLEFT;
                } else {
                    ui.playBump();
                }
                helpMePushGridPrease();
                break;
            case RIGHT:
                System.out.println("RIGHT");
                if (grid[sokoX+1][sokoY] != WALL) {
                    if (grid[sokoX+1][sokoY] == 5 || grid[sokoX+1][sokoY] == RED_DOT) {
                        grid[sokoX][sokoY] = underSokosFeet;
                        underSokosFeet = grid[sokoX+1][sokoY];
                        grid[sokoX+1][sokoY] = SOKO;
                    } else if (grid[sokoX+1][sokoY] == 2){
                        if (grid[sokoX+2][sokoY] == 5 || grid[sokoX+2][sokoY] == RED_DOT) {
                            grid[sokoX][sokoY] = underSokosFeet;
                            if (initGrid[sokoX+1][sokoY] == RED_DOT) {
                                underSokosFeet = RED_DOT;
                            } else {
                                underSokosFeet = EMPTY;
                            }
                            grid[sokoX+1][sokoY] = SOKO;
                            grid[sokoX+2][sokoY] = 2;

                        }else {
                            ui.playBump();
                        }
                    }
                    moveGrid[sokoX+1][sokoY] = WENTRIGHT;
                } else {
                    ui.playBump();
                }
                helpMePushGridPrease();
                break;
            default:
        }
        ui.render();
//        checkIfLost();
//        checkIfWon();
    }
    
    public void playSound(String filePath){
        
    }
    
    public void printGrid(){
        System.out.println("---GRID---");
        for (int i = 0; i < gridRows; i++) {
            for (int j = 0; j < gridColumns; j++) {
                System.out.print(grid[j][i] + " ");
            }
            System.out.println("");
        }
    }
    
    public void checkIfLost(){
        boolean youLost = false;
        for (int i = 0; i < gridColumns; i++) {
            for (int j = 0; j < gridRows; j++) {
                if (grid[i][j] == BOX) {
                    boolean bottomLeftLost = grid[i][j+1] == 1 && grid[i-1][j] == 1;
                    boolean bottomRightLost = grid[i][j+1] == 1 && grid[i+1][j] == 1;
                    boolean topRightLost = grid[i][j-1] == 1 && grid[i+1][j] == 1;
                    boolean topLeftLost = grid[i][j-1] == 1 && grid[i-1][j] == 1;
                    if ((bottomLeftLost || bottomRightLost || topRightLost || topLeftLost)&&(initGrid[i][j] != 3)) {
                        youLost = true;
                    }
                }
            }
        }
        if (youLost) {
            wonGame = !youLost;
            endGame();
            ui.showLoseDialog();
            ui.stopTheme();
            ui.playLoseSound();
        }
    }
    
    public void checkIfWon(){
       boolean youWon = true;
        outerloop:
        for (int i = 0; i < gridColumns; i++) {
            for (int j = 0; j < gridRows; j++) {
                if (grid[i][j] == 3) {
                    youWon = false;
                    break outerloop;
                } else if (underSokosFeet == RED_DOT){
                    youWon = false;
                    break outerloop;
                }
            }
        }
        if (youWon) {
            wonGame = youWon;
            endGame();
            ui.showWinDialog();
            ui.stopTheme();
            ui.playWinSound();
        }
    }
    
    public void undo(){
        System.out.println("undoStack size: " + undoStack.size());
        if(undoStack.size()>1){
            underSokosFeetStack.pop();
            underSokosFeet = (int)underSokosFeetStack.peek();
            undoStack.pop();
            grid = undoStack.peek();
        }
        ui.render();
    }
    
    private void helpMePushGridPrease(){
        int[][] newGrid = new int[grid.length][grid[0].length];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                newGrid[i][j] = grid[i][j];
            }
        }
        undoStack.push(newGrid);
        underSokosFeetStack.push(underSokosFeet);
    }
}
