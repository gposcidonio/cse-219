package sokoban.ui;

import java.util.ArrayList;

import sokoban.game.SokobanGameStateManager;
import application.Main.SokobanPropertyType;
import java.io.File;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import properties_manager.PropertiesManager;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;
import sokoban.file.SokobanFileLoader;
import sokoban.game.SokobanGameData;

public class SokobanUI extends Pane {

    /**
     * @return the sokoPixelX
     */
    public double getSokoPixelX() {
        return sokoPixelX;
    }

    /**
     * @return the sokoPixelY
     */
    public double getSokoPixelY() {
        return sokoPixelY;
    }

    /**
     * @param sokoPixelX the sokoPixelX to set
     */
    public void setSokoPixelX(double sokoPixelX) {
        this.sokoPixelX = sokoPixelX;
    }

    /**
     * @param sokoPixelY the sokoPixelY to set
     */
    public void setSokoPixelY(double sokoPixelY) {
        this.sokoPixelY = sokoPixelY;
    }

    /**
     * The SokobanUIState represents the three screen states that are possible
     * for the Sokoban game application. Depending on which state is in current
     * use, different controls will be visible.
     */
    public enum SokobanUIState {

        SPLASH_SCREEN_STATE, PLAY_GAME_STATE, VIEW_STATS_STATE, VIEW_HELP_STATE
    }

    // mainStage
    private Stage primaryStage;

    // mainPane
    private BorderPane mainPane;

    // SplashScreen
    private ImageView splashScreenImageView;
    private Pane splashScreenPane;
    private Label splashScreenImageLabel;
    private FlowPane levelSelectionPane;
    private ArrayList<Button> levelButtons;

    // NorthToolBar
    private HBox northToolbar;
    private Button gameButton;
    private Button statsButton;
    private Button undoButton;
    private Label timerLabel;

    // GamePane
    private Label SokobanLabel;
    private Button newGameButton;
    private BorderPane gamePanel = new BorderPane();

    //StatsPane
    private Pane statsPane;

    private Pane workspace;

    // Padding
    private Insets marginlessInsets;

    // Image path
    private String ImgPath = "file:images/";

    // mainPane weight && height
    private int paneWidth;
    private int paneHeight;

    // THIS CLASS WILL HANDLE ALL ACTION EVENTS FOR THIS PROGRAM
    private SokobanEventHandler eventHandler;
    private SokobanErrorHandler errorHandler;
    private SokobanDocumentManager docManager;

    //RENDERER AND GRAPHICS CONTEXT
    public GridRenderer gridRenderer;
    private GraphicsContext gc;

    //GAME DATA
    public SokobanGameData gameData;

    //TIMLINE
    Timeline timeline;

    private static double sokoPixelX;
    private static double sokoPixelY;
    
    private static Media bump; 
    private static Media winSound;
    private static Media loseSound;
    private static Media themeSong;
    private static MediaPlayer themePlayer;

    SokobanGameStateManager gsm;
    
    public static boolean sokoIsMovingYo;
    public static double horizShift;
    public static double vertShift;
    public static double horizOffset;
    public static double vertOffset;
    

    public SokobanUI() {
        gsm = new SokobanGameStateManager(this);
        eventHandler = new SokobanEventHandler(this);
        errorHandler = new SokobanErrorHandler(primaryStage);
        docManager = new SokobanDocumentManager(this);
        bump = new Media(new File("smb_bump.wav").toURI().toString());
        winSound = new Media(new File("smb_stage_clear.wav").toURI().toString());
        loseSound = new Media(new File("smb_mariodie.wav").toURI().toString());
        themeSong = new Media(new File("smb_theme.mp3").toURI().toString());
        horizShift = 0;
        vertShift = 0;
        setTheme();
        initMainPane();
        initSplashScreen();

    }

    public void SetStage(Stage stage) {
        primaryStage = stage;
    }

    public BorderPane GetMainPane() {
        return this.mainPane;
    }

    public SokobanGameStateManager getGSM() {
        return gsm;
    }

    public SokobanDocumentManager getDocManager() {
        return docManager;
    }

    public SokobanErrorHandler getErrorHandler() {
        return errorHandler;
    }

    public void initMainPane() {
        marginlessInsets = new Insets(5, 5, 5, 5);
        mainPane = new BorderPane();

        PropertiesManager props = PropertiesManager.getPropertiesManager();
        paneWidth = Integer.parseInt(props
                .getProperty(SokobanPropertyType.WINDOW_WIDTH));
        paneHeight = Integer.parseInt(props
                .getProperty(SokobanPropertyType.WINDOW_HEIGHT));
        mainPane.resize(paneWidth, paneHeight);
//        mainPane.setPadding(marginlessInsets);
    }

    public void viewSplashScreen() {
        mainPane.getChildren().clear();
        mainPane.setCenter(splashScreenPane);
    }

    public void initSplashScreen() {

        themePlayer.play();
        // INIT THE SPLASH SCREEN CONTROLS
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String splashScreenImagePath = props
                .getProperty(SokobanPropertyType.SPLASH_SCREEN_IMAGE_NAME);
        props.addProperty(SokobanPropertyType.INSETS, "5");
        String str = props.getProperty(SokobanPropertyType.INSETS);

        splashScreenPane = new StackPane();

        Image splashScreenImage = loadImage(splashScreenImagePath);
        splashScreenImageView = new ImageView(splashScreenImage);

        splashScreenImageLabel = new Label();
        splashScreenImageLabel.setGraphic(splashScreenImageView);
        // move the label position to fix the pane
        splashScreenImageLabel.setLayoutX(-45);
        splashScreenPane.getChildren().add(splashScreenImageLabel);

        // GET THE LIST OF LEVEL OPTIONS
        ArrayList<String> levels = props
                .getPropertyOptionsList(SokobanPropertyType.LEVEL_OPTIONS);
        ArrayList<String> levelImages = props
                .getPropertyOptionsList(SokobanPropertyType.LEVEL_IMAGE_NAMES);
        ArrayList<String> levelFiles = props
                .getPropertyOptionsList(SokobanPropertyType.LEVEL_FILES);

        levelSelectionPane = new FlowPane();
        levelSelectionPane.setAlignment(Pos.CENTER);
        // add key listener
        levelButtons = new ArrayList<Button>();
        for (int i = 0; i < levels.size(); i++) {

            // GET THE LIST OF LEVEL OPTIONS
            String level = levels.get(i);
            String levelImageName = levelImages.get(i);
            Image levelImage = loadImage(levelImageName);
            ImageView levelImageView = new ImageView(levelImage);
            levelImageView.setFitHeight(139);
            levelImageView.setFitHeight(139);

            // AND BUILD THE BUTTON
            Button levelButton = new Button();
            levelButton.setGraphic(levelImageView);

            // CONNECT THE BUTTON TO THE EVENT HANDLER
            levelButton.setOnAction((ActionEvent event) -> {
                eventHandler.respondToSelectLevelRequest(level);
            });
            // TODO
            levelSelectionPane.getChildren().add(levelButton);
            // TODO: enable only the first level

        }

        splashScreenPane.getChildren().add(levelSelectionPane);
        mainPane.setCenter(splashScreenPane);

    }

    /**
     * This method initializes the language-specific game controls, which
     * includes the three primary game screens.
     */
    public void initSokobanUI() {
        // FIRST REMOVE THE SPLASH SCREEN
        mainPane.getChildren().clear();

        // GET THE UPDATED TITLE
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String title = props.getProperty(SokobanPropertyType.GAME_TITLE_TEXT);
        primaryStage.setTitle(title);

        // THEN ADD ALL THE STUFF WE MIGHT NOW USE
        initNorthToolbar();

        // OUR WORKSPACE WILL STORE EITHER THE GAME, STATS,
        // OR SPLASH SCREEN UI AT ANY ONE TIME
        initWorkspace();
        initGameScreen();
        //initStatsPane();

        // WE'LL START OUT WITH THE GAME SCREEN
        changeWorkspace(SokobanUIState.PLAY_GAME_STATE);

    }

    public void initStatsPane(){
        statsPane = new Pane();
        gameData = gsm.getGameData();
        double width = paneWidth;
        double height = paneHeight - northToolbar.getHeight();
        TextArea textField = new TextArea();
        textField.setEditable(false);
        textField.setStyle(""
                + "-fx-font-size:30px;"
                + "-fx-font-family:monospace;");
        textField.setPrefHeight(height);
        textField.setPrefWidth(width);
        String lvl = "" + gameData.getLevelNumber();
        String games = "" + gameData.getGamesPlayed();
        String wins = "" + gameData.getWins();
        String losses = "" + gameData.getLosses();
        String winRate = "" + gameData.getWinRate();
        String fastestWin = ""; 
        if (gameData.getFastestWin() != -1) {
            long timeInMillis = gameData.getFastestWin();
            long hours = timeInMillis / (1000*60*60);
            timeInMillis -= hours * (1000*60*60);
            long minutes = timeInMillis / (1000*60);
            timeInMillis -= minutes * (1000*60);
            long seconds = timeInMillis / (1000);
             String minutesText = "" + minutes;
            if (minutes < 10) {
                minutesText = "0" + minutesText;
            }
            String secondsText = "" + seconds;
            if (seconds < 10) {
                secondsText = "0" + secondsText;
            }
            fastestWin += hours + ":" + minutesText + ":" + secondsText;
        } else {
            fastestWin += "N/A";
        }
        String output = ""
                + "Level:\t\t" + lvl
                + "\nGames Played:\t" + games
                + "\nWins:\t\t" + wins
                + "\nLosses:\t\t" + losses
                + "\nWin Rate:\t" + winRate
                + "\nFastest Win:\t" + fastestWin;
        textField.setText(output);
        statsPane.getChildren().add(textField);
        mainPane.setCenter(statsPane);
    }
    /**
     * This function initializes all the controls that go in the north toolbar.
     */
    private void initNorthToolbar() {
        // MAKE THE NORTH TOOLBAR, WHICH WILL HAVE FOUR BUTTONS
        northToolbar = new HBox();
        northToolbar.setStyle("-fx-background-color:lightgray");
        northToolbar.setAlignment(Pos.CENTER);
        northToolbar.setPadding(marginlessInsets);
        northToolbar.setSpacing(10.0);

        // MAKE AND INIT THE GAME BUTTON
        gameButton = initToolbarButton(northToolbar,
                "BACK");
        //setTooltip(gameButton, SokobanPropertyType.GAME_TOOLTIP);
        gameButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                // TODO Auto-generated method stub
                eventHandler.respondToBackRequest();
            }
        });

        // MAKE AND INIT THE STATS BUTTON
        statsButton = initToolbarButton(northToolbar,
                "STAT");
        //setTooltip(statsButton, SokobanPropertyType.STATS_TOOLTIP);

        statsButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                // TODO Auto-generated method stub
                eventHandler
                        .respondToOpenStats();
            }

        });

        // MAKE AND INIT THE EXIT BUTTON
        undoButton = initToolbarButton(northToolbar,
                "UNDO");
        undoButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                // TODO Auto-generated method stub
                eventHandler.respondToUndoRequest();
            }

        });

        timerLabel = new Label();
        northToolbar.getChildren().add(timerLabel);

        // AND NOW PUT THE NORTH TOOLBAR IN THE FRAME
        mainPane.setTop(northToolbar);
        //mainPane.getChildren().add(northToolbar);
    }

    /**
     * This method helps to initialize buttons for a simple toolbar.
     *
     * @param toolbar The toolbar for which to add the button.
     *
     * @param prop The property for the button we are building. This will
     * dictate which image to use for the button.
     *
     * @return A constructed button initialized and added to the toolbar.
     */
    private Button initToolbarButton(HBox toolbar, String prop) {
        // GET THE NAME OF THE IMAGE, WE DO THIS BECAUSE THE
        // IMAGES WILL BE NAMED DIFFERENT THINGS FOR DIFFERENT LANGUAGES
//        PropertiesManager props = PropertiesManager.getPropertiesManager();
//        String imageName = props.getProperty(prop);

        // LOAD THE IMAGE
//        Image image = loadImage(prop);
//        ImageView imageIcon = new ImageView(image);

        // MAKE THE BUTTON
        Button button = new Button();
        button.setText(prop);
        button.setPadding(marginlessInsets);

        // PUT IT IN THE TOOLBAR
        toolbar.getChildren().add(button);

        // AND SEND BACK THE BUTTON
        return button;
    }

    public void initGameScreen() {
        changeWorkspace(SokobanUIState.PLAY_GAME_STATE);
        initNorthToolbar();
    }

    /**
     * The workspace is a panel that will show different screens depending on
     * the user's requests.
     */
    private void initWorkspace() {
        // THE WORKSPACE WILL GO IN THE CENTER OF THE WINDOW, UNDER THE NORTH
        // TOOLBAR
        workspace = new StackPane();
        mainPane.setCenter(workspace);

        //mainPane.getChildren().add(workspace);
        System.out.println("in the initWorkspace");
    }

    public Image loadImage(String imageName) {
        Image img = new Image(ImgPath + imageName);
        return img;
    }

    /**
     * This function selects the UI screen to display based on the uiScreen
     * argument. Note that we have 3 such screens: game, stats, and help.
     *
     * @param uiScreen The screen to be switched to.
     */
    public void changeWorkspace(SokobanUIState uiScreen) {
        switch (uiScreen) {
            case PLAY_GAME_STATE:
                mainPane.setCenter(gamePanel);
                break;
            case VIEW_STATS_STATE:
                mainPane.setCenter(statsPane);
                break;
            default:
        }

    }

    public void startLevel(String level) {
        
        gsm.makeNewGame("./data/level" + level.substring(level.length() - 1, level.length()) + ".sav");
        gameData = gsm.getGameData();
        if (gameData == null) {
            gameData = new SokobanGameData(Integer.parseInt(level.substring(level.length() - 1, level.length())));
            gsm.setGameData(gameData);
            System.out.println("in startLevel. GameData is null");
        }
        String filePath = "./data/level" + level.substring(level.length() - 1, level.length()) + ".sok";
        resetTimer();
        setTimer();
        //READ FILE INTO GSM
        SokobanFileLoader fileLoader = new SokobanFileLoader(filePath, gsm);

        double width = paneWidth;
        double height = paneHeight - northToolbar.getHeight();
        gridRenderer = new GridRenderer(width, height);
        gamePanel.setCenter(gridRenderer);

        mainPane.setOnKeyPressed((KeyEvent ke) -> eventHandler.respondToArrowKey(ke));
        mainPane.setOnMousePressed((MouseEvent me) -> eventHandler.respondToMousePressed(me));
        mainPane.setOnMouseReleased((MouseEvent me) -> eventHandler.respondToMouseReleased(me));
        mainPane.setOnMouseClicked((MouseEvent me) -> eventHandler.respondToMouseClicked(me));

    }

    public class GridRenderer extends Canvas {

        // PIXEL DIMENSIONS OF EACH CELL
        int cellWidth;
        int cellHeight;

        // images
        Image wallImage = new Image("file:images/wall.png");
        Image boxImage = new Image("file:images/box.png");
        Image placeImage = new Image("file:images/place.png");
        Image sokobanImage = new Image("file:images/Sokoban.png");

        /**
         * Default constructor.
         */
        public GridRenderer() {
            this.setWidth(500);
            this.setHeight(500);
            repaint();
        }
        
        public GridRenderer(double x, double y) {
            this.setWidth(x);
            this.setHeight(y);
            repaint();
        }
        
        public void paintWinLoss(String textToDraw){
            gc = this.getGraphicsContext2D();
            gc.setFont(Font.font("monospace",500));
            gc.setFill(Color.BLACK);
            gc.fillText(textToDraw, 0, this.getHeight() - 100,this.getWidth());
        }
        

        public void repaint() {
            gc = this.getGraphicsContext2D();
            gc.clearRect(0, 0, this.getWidth(), this.getHeight());

            // CALCULATE THE GRID CELL DIMENSIONS
            double w = this.getWidth() / gsm.getGridColumns();
            double h = w;

            gc = this.getGraphicsContext2D();
            if (false)
            if (sokoIsMovingYo) {
                horizShift = h/15;
                vertShift = h/15;
                horizOffset = h;
                vertOffset = h;
            }

            // NOW RENDER EACH CELL
            int x = 0, y = 0;
            for (int i = 0; i < gsm.getGridColumns(); i++) {
                y = 0;
                for (int j = 0; j < gsm.getGridRows(); j++) {
                    // DRAW THE CELL
                    gc.setFill(Color.LIGHTBLUE);
                    gc.strokeRoundRect(x, y, w, h, 10, 10);

                    switch (gsm.getGrid()[i][j]) {
                        case 0:
                            gc.strokeRoundRect(x, y, w, h, 10, 10);
                            break;
                        case 1:
                            gc.drawImage(wallImage, x, y, w, h);
                            break;
                        case 2:
                            gc.drawImage(boxImage, x, y, w, h);
                            break;
                        case 3:
                            gc.drawImage(placeImage, x, y, w, h);
                            break;
                        case 4:
                            gsm.setSokoX(i);
                            gsm.setSokoY(j);
                            gc.drawImage(sokobanImage, x, y, w, h);
                            setSokosPixelLocation(x,y,w,h);
                            break;
                    }

                    // ON TO THE NEXT ROW
                    y += h;
                }
                // ON TO THE NEXT COLUMN
                x += w;
            }
        }
        

    }

    public void render() {
        gridRenderer.repaint();
    }

    public void setTimer() {
        timerLabel.setText(gameData.toString());
        timeline = new Timeline(new KeyFrame(Duration.millis(1), tm -> updateTimer()));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }
    
    public void resetTimer(){
        gameData.resetTime();
    }

    public void updateTimer() {
        timerLabel.setText(gameData.toString());
    }

    public static void setSokosPixelLocation(int x, int y, double w, double h) {
        sokoPixelX = x + (w/2);
        sokoPixelY = y + (h/2) + 36;
    }
    
    public void showWinDialog(){
        gridRenderer.paintWinLoss("YOU WIN");
    }
    public void showLoseDialog(){
        gridRenderer.paintWinLoss("YOU LOSE");
    }
    
    public void playBump(){
        MediaPlayer player = new MediaPlayer(bump);
        player.setCycleCount(1);
        player.play();
    }
    
    public void playWinSound(){
        MediaPlayer player = new MediaPlayer(winSound);
        player.setCycleCount(1);
        player.play();
    }
    
    public void playLoseSound(){
        MediaPlayer player = new MediaPlayer(loseSound);
        player.setCycleCount(1);
        player.play();
    }
    
    public void setTheme(){
        themePlayer = new MediaPlayer(themeSong);
        themePlayer.setCycleCount(MediaPlayer.INDEFINITE);
    }
    
    public void playTheme(){
        themePlayer.play();
    }
    
    public void stopTheme(){
        themePlayer.stop();
    }
}
