package sokoban.ui;


import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import application.Main.SokobanPropertyType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import properties_manager.PropertiesManager;
import sokoban.game.SokobanGameStateManager;

public class SokobanEventHandler {

    private SokobanUI ui;
    private SokobanGameStateManager gsm;

    private double xInitPos;
    private double yInitPos;
    private double xFinalPos;
    private double yFinalPos;
    /**
     * Constructor that simply saves the ui for later.
     *
     * @param initUI
     */
    public SokobanEventHandler(SokobanUI initUI) {
        ui = initUI;
        gsm = ui.getGSM();
    }

    /**
     * This method responds to when the user wishes to switch between the Game,
     * Stats, and Help screens.
     *
     * @param uiState The ui state, or screen, that the user wishes to switch
     * to.
     */
    public void respondToOpenStats() {
        ui.initStatsPane();
    }

    
    public void respondToBackRequest(){
        gsm.endGame();
        ui.GetMainPane().getChildren().clear();
        ui.initSplashScreen();
    }

    /**
     * This method responds to when the user requests to exit the application.
     *
     * @param window The window that the user has requested to close.
     */
    public void respondToExitRequest(Stage primaryStage) {
        // ENGLIS IS THE DEFAULT
        String options[] = new String[]{"Yes", "No"};
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        options[0] = props.getProperty(SokobanPropertyType.DEFAULT_YES_TEXT);
        options[1] = props.getProperty(SokobanPropertyType.DEFAULT_NO_TEXT);
        String verifyExit = props.getProperty(SokobanPropertyType.DEFAULT_EXIT_TEXT);

        // NOW WE'LL CHECK TO SEE IF LANGUAGE SPECIFIC VALUES HAVE BEEN SET
        if (props.getProperty(SokobanPropertyType.YES_TEXT) != null) {
            options[0] = props.getProperty(SokobanPropertyType.YES_TEXT);
            options[1] = props.getProperty(SokobanPropertyType.NO_TEXT);
            verifyExit = props.getProperty(SokobanPropertyType.EXIT_REQUEST_TEXT);
        }

        // FIRST MAKE SURE THE USER REALLY WANTS TO EXIT
        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        BorderPane exitPane = new BorderPane();
        HBox optionPane = new HBox();
        Button yesButton = new Button(options[0]);
        Button noButton = new Button(options[1]);
        optionPane.setSpacing(10.0);
        optionPane.getChildren().addAll(yesButton, noButton);
        Label exitLabel = new Label(verifyExit);
        exitPane.setCenter(exitLabel);
        exitPane.setBottom(optionPane);
        Scene scene = new Scene(exitPane, 50, 100);
        dialogStage.setScene(scene);
        dialogStage.show();
        // WHAT'S THE USER'S DECISION?
        yesButton.setOnAction(e -> {
            // YES, LET'S EXIT
            System.exit(0);
        });
        noButton.setOnAction(e -> {
            dialogStage.close();
        });

    }
    
    public void respondToSelectLevelRequest(String level){
        gsm.resetCorrectSpots();
        ui.initGameScreen();
        ui.startLevel(level);
        
    }
    
    public void respondToUndoRequest(){
        gsm.undo();
    }
    
    public void respondToArrowKey(KeyEvent ke){
        switch(ke.getCode()){
            case UP:
                gsm.resolveDatMoveDoe(KeyCode.UP);
                break;
            case DOWN:
                gsm.resolveDatMoveDoe(KeyCode.DOWN);
                break;
            case LEFT:
                gsm.resolveDatMoveDoe(KeyCode.LEFT);
                break;
            case RIGHT:
                gsm.resolveDatMoveDoe(KeyCode.RIGHT);
                break;
            default:
                
        }
        
    }
    
    public void respondToMousePressed(MouseEvent me){
        xInitPos = me.getSceneX();
        yInitPos = me.getSceneY();
        
    }
    
    public void respondToMouseReleased(MouseEvent me){
        xFinalPos = me.getSceneX();
        yFinalPos = me.getSceneY();
        double slope = (yFinalPos - yInitPos) / (xFinalPos - xInitPos);

        boolean wentUp = (slope < -1 | slope > 1) & (yFinalPos < yInitPos);
        boolean wentRight = (slope < 1 & slope > -1) & (xFinalPos > xInitPos);
        boolean wentDown = (slope < -1 | slope > 1) & (yFinalPos > yInitPos);
        boolean wentLeft = (slope < 1 & slope > -1) & (xFinalPos < xInitPos);

        if (wentUp) {
            gsm.resolveDatMoveDoe(KeyCode.UP);
        } else if (wentRight) {
            gsm.resolveDatMoveDoe(KeyCode.RIGHT);
        } else if (wentDown){
            gsm.resolveDatMoveDoe(KeyCode.DOWN);
        } else if (wentLeft){
            gsm.resolveDatMoveDoe(KeyCode.LEFT);
        }
            
    }
    
    public void respondToMouseClicked(MouseEvent me){
        if(me.getClickCount()==2){
            double xPos = me.getSceneX();
            double yPos = me.getSceneY();
            double slope = (yPos - ui.getSokoPixelY()) / (xPos - ui.getSokoPixelX());
            
            boolean wentUp = (slope < -1 | slope > 1) & (yPos < ui.getSokoPixelY());
            boolean wentRight = (slope < 1 & slope > -1) & (xPos > ui.getSokoPixelX());
            boolean wentDown = (slope < -1 | slope > 1) & (yPos > ui.getSokoPixelY());
            boolean wentLeft = (slope < 1 & slope > -1) & (xPos < ui.getSokoPixelX());

            if (wentUp) {
                gsm.resolveDatMoveDoe(KeyCode.UP);
            } else if (wentRight) {
                gsm.resolveDatMoveDoe(KeyCode.RIGHT);
            } else if (wentDown){
                gsm.resolveDatMoveDoe(KeyCode.DOWN);
            } else if (wentLeft){
                gsm.resolveDatMoveDoe(KeyCode.LEFT);
            }
        }
    }

}
