package sokoban.file;

import java.io.IOException;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import sokoban.game.SokobanGameData;
import sokoban.game.SokobanGameStateManager;

public class SokobanFileLoader {

    private int[][] grid;
    private int gridRows;
    private int gridColumns;
    private int numRedDots;
    
    
    public SokobanFileLoader(String filePath,SokobanGameStateManager gsm){
        numRedDots = 0;
        File fileToOpen = new File(filePath);
        try{
            byte[] bytes = new byte[Long.valueOf(fileToOpen.length()).intValue()];
            ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
            FileInputStream fis = new FileInputStream(fileToOpen);
            BufferedInputStream bis = new BufferedInputStream(fis);
            
            bis.read(bytes);
            bis.close();
            
            DataInputStream dis = new DataInputStream(bais);
            
            int initGridColumns = dis.readInt();
            int initGridRows = dis.readInt();
            int[][] newGrid = new int[initGridColumns][initGridRows];
            
            for (int i = 0; i < initGridColumns; i++) {
                for (int j = 0; j < initGridRows; j++) {
                    newGrid[i][j] = dis.readInt();
                    if (newGrid[i][j] == 3) {
                        numRedDots++;
                    }
                }
            }
            
            grid = newGrid;
            gridRows = initGridRows;
            gridColumns = initGridColumns;
           
            
        } catch (FileNotFoundException f){
            f.printStackTrace();
        } catch (IOException i){
            i.printStackTrace();
        }
        
        gsm.newGrid(gridRows, gridColumns, grid);
        gsm.setNumRedDots(numRedDots);
    }
    
    public SokobanFileLoader(String filePath, SokobanGameData gameData, SokobanGameStateManager gsm){
        File fileToOpen = new File(filePath);
        if (!fileToOpen.exists()) {
            System.out.println("this");
            try{
                fileToOpen.createNewFile();
            } catch (IOException ex){
                //ignore
            }
            int levelNumber = Integer.parseInt(filePath.substring(filePath.length()-5, filePath.length()-4));
                int gamesPlayed = 0;
                int gamesWon = 0;
                int gamesLost = 0;
                double winRate = 0;
                long fastestWin = -1;
                System.out.println("this should not print");
//                System.out.println(levelNumber +" "+ gamesPlayed);
                gameData = new SokobanGameData(levelNumber, gamesPlayed, gamesWon, gamesLost, winRate, fastestWin);
                gsm.setGameData(gameData);
        } else {
            System.out.println("that");
            try{
                byte[] bytes = new byte[Long.valueOf(fileToOpen.length()).intValue()];
                ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
                FileInputStream fis = new FileInputStream(fileToOpen);
                BufferedInputStream bis = new BufferedInputStream(fis);

                bis.read(bytes);
                bis.close();

                DataInputStream dis = new DataInputStream(bais);
                System.out.println("the code gets here");
                int levelNumber = dis.readInt();
                int gamesPlayed = dis.readInt();
                int gamesWon = dis.readInt();
                int gamesLost = dis.readInt();
                double winRate = dis.readDouble();
                long fastestWin = dis.readLong();
                System.out.println(levelNumber +" "+ gamesPlayed);
                gameData = new SokobanGameData(levelNumber, gamesPlayed, gamesWon, gamesLost, winRate, fastestWin);
                gsm.setGameData(gameData);
                fis.close();
                dis.close();
                bais.close();
                
            } catch (FileNotFoundException f){
                f.printStackTrace();
            } catch (IOException i){
                i.printStackTrace();
            }
        }
    }
    
    public int[][] getGrid(){
     return grid;
    }
    
    public int getGridRows(){
        return gridRows;
    }
    
    public int getGridColumns(){
        return gridColumns;
    }
    
    public int getNumRedDots(){
        return numRedDots;
    }
    
}
